#!/usr/bin/env python
from Record import Record
class Student:
    
    record=None

    def __init__(self, (studentID, username, password, fname, lname, birthday, comments, category), record):
        self.id = studentID
        self.username = username
        self.password = password
        self.fname = fname
        self.lname = lname
        self.birthday = birthday
        self.comments = comments
        self.record = record
        self.category = category
    
    def getInfo(self):
        """Return a dict containing the basic information about a student"""
        return {'id': self.id, 'username': self.username, 'passwordHash': self.password, 'fname': self.fname, 'lname':self.lname, 'birthday':self.birthday, 'comments':self.comments, 'category':self.category}

    def setRecord(self, record):
        self.record = record

    def getRecord(self):
        return self.record
