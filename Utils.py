#!/usr/bin/env python
from functools import partial
import subprocess, os, signal, tkMessageBox
from Tkinter import Tk, Frame, Listbox, Button, Entry, Label, Message, Scrollbar, OptionMenu, TclError
from Tkconstants import FLAT, LEFT, BOTH, RIGHT, Y, END
import sqlite3, hashlib
from datetime import date
from Classes import Student, Record
class FestivalInterface:
    #The FetivalObject contructor creating the popen object
    #and setting the audio mode on festival so that it doesn't close after one (Say Text) command. 
    #A problem on Natty Narwhal
    def __init__(self):
        try:
            self.p = subprocess.Popen(["festival", "--pipe"], stdin=subprocess.PIPE, preexec_fn=os.setsid)
        except OSError:
            tkMessageBox.showerror("Festival Error", "You either don't have festival installed or it is not in your path.")
            exit()
        
        self.p.stdin.write("(audio_mode 'async)\n")
    #The main function of the class, the one that causes text to be sent to festival along the pipe
    def speech(self, text):
        #Make sure the input isn't empty
        if (text == ""):
            text = "You didn't enter anything"
        #Print to the command line what is being spoken
        #print "Speaking: " + str(text)
        #Create the text to send through the pipe to festival
        text = "(SayText \"" + str(text) + "\")\n"
        #send data along the pipe
        self.p.stdin.write(text)
    
    #The function that kills the current fetival process and its children processes.
    #It also recreates the pipe on a new festival session
    def resetSpeech(self):
        #Indicate on the command line that the 'stop' button has been pressed
        print "Killing"
        #Send SIGKILL to the process group of the current instance of festival
	os.killpg(os.getpgid(self.p.pid), signal.SIGKILL)
        #Reopen festival
        self.__init__()



class ListPane(Frame):
    """A ListPane is a widget for holding a list of words, incorporating a scrollbar"""
    
    def __init__(self, parent, height=35, width=40):
        Frame.__init__(self, parent)
        
        #Create a scrollbar so that list can be of arbitrary length
        scrollbar = Scrollbar(self)
        #Create a listbox to hold/display the word list
        self.listbox  = Listbox(self, height = height, width = width, 
        yscrollcommand = scrollbar.set)
        scrollbar.config(command = self.listbox.yview)
        self.listbox.pack(side=LEFT, fill = BOTH)
        scrollbar.pack(side=RIGHT, fill = Y)
        
        #Allow the widget to react to single or double clicks
        self.listbox.bind("<Double-Button-1>", self.doubleclicked)
        self.listbox.bind("<Button-1>", self.singleclicked)

    def remove(self, itemindex):
        """Removes the word at the given index of the list"""
        self.listbox.delete(itemindex, itemindex)

    def get(self, index=None):
        """Get the currently selected word"""
        try:
            if index!=None:
                return self.listbox.get(index)
            else: return self.get(self.listbox.curselection())
        except TclError:
            return None

    def getDisplayed(self):
        """Get a list of all words currently contained in the ListPane"""
        return self.listbox.get(0,END)

    def insert(self, item):
        """Insert a word into the list"""
        self.listbox.insert(END, item) 

    def display(self, itemlist):
        """Pass a list to this method and the contents will be displayed in the ListPane"""
        self.listbox.delete(0, END)
        for item in itemlist:
            self.listbox.insert(END, item)

    def doubleclicked(self, event):
        """Called when widget is double-clicked. Template method that calls doubleClickFunc"""
        try:
            itemindex = self.listbox.curselection()
            self.doubleClickFunc(itemindex)
        except TclError:
            pass
    
    def doubleClickFunc(self, itemindex):
        """This method should be replaced on specific instances of ListPane. 
           Defines the behaviour of widget when double-clicked"""
        pass
    
    def singleclicked(self, event):
        """Called when widget is single-clicked. Template method that calls singleClickFunc"""
        self.singleClickFunc(self.listbox.index("@%d,%d" % (event.x, event.y)))

    def singleClickFunc(self, itemindex):
        """This method should be replaced on specific instances of ListPane
           Defines the behaviour of widget when single-clicked"""
        pass


class TldrParser(object):
    '''Class for importing and exporting .tldr files''' 
    def __init__(self, tldrFile=None):
        '''Constructor calls readtldr if filename given
        Makes assumption that file is of right format.
        Fix needed'''
        
        self.wordlist = {}
        if tldrFile != None:
            self.readtldr(tldrFile)

    def readtldr(self, filename):
        f = open(filename)
        line_count = 0
        for line in f:
            #Get meta information from first 3 lines. Assumes correct formatting
            if line_count == 0:
                self.source = line.strip('#\n')
            elif line_count == 1:
                self.date = line.strip('#\n')
            elif line_count == 2:
                self.num_words = int(line.strip('#\n'))
            else:
                if not line.startswith('#'):
                    #comments are ignored
                    line = line.strip('\n')
                    wordline = line.split('|')
                    self.wordlist[wordline[0]] = [wordline[1], wordline[2], wordline[3]]

            line_count += 1

        f.close()
    
    def writetldr(self, filename):
        #Exports the tldr object to a file.
        #Overwrites existing file.
        f = open(filename, 'w')
        f.write(self.source)
        f.write(self.date)
        f.write(self.num_words)
        for word in self.wordlist.keys():
            line = "%s|%s|%s|%s\n" % (word, self.wordlist[word][0], self.wordlist[word][1], self.wordlist[word][2])
            f.write(line)
        f.close()
    
    
    '''Getters and Setters. 
    Getters should be usually only used after a readtldr() call. 
    Setters should typically used before calling writetldr().
    Setting should be enabled in constructor. Needs doing'''
    
    def getWordList(self):
        #returns a dict with a 3 item list as the value
        return self.wordlist

    def getListSize(self):
        return self.num_words

    def getDateEdited(self):
        return self.date

    def getSource(self):
        return self.source

    def setWordList(self, wordlist):
        #Takes a dict with a 3 item list as value 
        self.wordlist = wordlist

    def setListSize(self, size):
        self.num_words = "#%s\n" % (size)

    def setDateEdited(self, date_string):
        self.date = "#%s\n" % (date_string)

    def setSource(self, source):
        self.source = "#%s\n" % (source)

class SpellingDatabase(object):
    """Is an interface to an sqlite database that contains tables specific to this application"""
    def __init__(self):
        self.db = sqlite3.connect('spelling.db')
        self.db.row_factory = sqlite3.Row
        self.cursor = self.db.cursor()
        database_check = self.sql("SELECT name FROM sqlite_master WHERE type='table'")
        #Check if there is a database that already exists.
        #Create the tables if the number of tables isnt what it should be.
        if len(database_check) == 0:
            self.sql("""CREATE TABLE lists (list_id INTEGER PRIMARY KEY,
                                            list_name TEXT,
                                            source TEXT,
                                            date_edited TEXT,
                                            num_words NUMERIC,
                                            difficulty TEXT)""")

            self.sql("""CREATE TABLE students (student_id INTEGER PRIMARY KEY,
                                               username TEXT,
                                               password TEXT,
                                               first_name TEXT,
                                               last_name TEXT,
                                               birthday TEXT,
                                               comments TEXT,
                                               category TEXT)""")

            self.sql("""CREATE TABLE Records(student_id INTEGER,
                                             list_id INTEGER,
                                             best_time NUMERIC,
                                             high_score NUMERIC,
                                             completion NUMERIC)""")
            
            self.sql("""CREATE TABLE wordCompletion(student_id INTEGER,
                                             word_id INTEGER,
                                             completed BOOLEAN,
                                             attempts NUMERIC)""")
            
            self.sql("""CREATE TABLE word_list_map (word_id INTEGER, 
                                                    list_id INTEGER)""")

            self.sql("""CREATE TABLE words (word_id INTEGER PRIMARY KEY,
                                            word TEXT,
                                            definition TEXT,
                                            usage TEXT,
                                           difficulty TEXT)""")

    #Method for directly quering the database
    def sql(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()
    
    #Method for handling the importing of lists
    def importList(self, listName, wordDict, source, date_edited, num_words):
        #check if list with same name already exists
        self.cursor.execute("select list_id from lists where list_name = '%s';" % (listName))
        word_check = self.cursor.fetchall()
        if len(word_check) > 0:
            #return 0 if listName is already taken
            return 0
        #else create a record in the database
        self.sql("""insert into lists (list_name, source, date_edited, num_words, category) 
                    values ('%s', '%s', '%s', '%s', 'UL1');""" % (self.duplicateSingleQuotes(listName),                                                        
                                                           self.duplicateSingleQuotes(source),
                                                           self.duplicateSingleQuotes(date_edited),
                                                           self.duplicateSingleQuotes(num_words)))
        #Find the id of the list that we just isnerted
        list_id = self.cursor.lastrowid
        for word in wordDict.keys():
            #Check if word is already in database
            word_check = self.sql("SELECT word_id FROM words WHERE word = '%s' AND difficulty = '%s';" % (self.duplicateSingleQuotes(word), self.duplicateSingleQuotes(wordDict[word][2])))
            #If not insert in table
            if len(word_check) == 0:
                self.sql("""INSERT INTO words ('word','definition','usage','difficulty') 
                          VALUES ('%s','%s','%s','%s');""" % (self.duplicateSingleQuotes(word),
                                                            self.duplicateSingleQuotes(wordDict[word][0]),
                                                            self.duplicateSingleQuotes(wordDict[word][1]),
                                                            self.duplicateSingleQuotes(wordDict[word][2])))
                word_id = self.cursor.lastrowid
            #else only insert into the word list map
            else:
                word_id = word_check[0][0]
            
            self.sql("""INSERT INTO word_list_map ('word_id', 'list_id') 
                      VALUES ('%s', '%s');""" % (word_id, list_id))
        
        #Indicate that insertion was successful
        return 1
    #Method for creating lists. Used in the teacher interface
    def createList(self, word_list, list_name, category):
        self.sql("INSERT INTO lists (list_name, source, date_edited, num_words) VALUES ('%s', '%s', '%s', '%d')"%(list_name, "User Created List", str(date.today()), len(word_list)))
        list_id = self.cursor.lastrowid
        for word in word_list:
            id_record = self.sql("SELECT word_id FROM words WHERE word = '%s'"%(word))
            word_id = id_record[0][0]
            self.sql("INSERT INTO word_list_map VALUES ('%d', '%d')"%(word_id, list_id))
    #Some getters
    def getList(self, list_name):
        return self.sql("SELECT * FROM lists where list_name = '%s'" % (list_name))

    
    def getAllWords(self):
        return self.sql("SELECT * FROM words")

    def getWords(self, listName):
        return self.sql("""SELECT words.word, words.definition, words.usage, words.difficulty, words.word_id FROM words, lists, word_list_map where lists.list_id = word_list_map.list_id 
                                                            and words.word_id = word_list_map.word_id
                                                            and lists.list_name = '%s'""" % (listName))
    def getWord(self, wordName):
        return self.sql("SELECT * from words where word='%s'" % (self.duplicateSingleQuotes(wordName)))

    def getLists(self):
        return self.sql("SELECT * FROM lists")
    #decontructor trys to cleanlt close the databse connection
    def __del__(self):
        self.db.commit()
        self.db.close()

    #Modify the details of a student
    def update_student(self, old_fname, old_lname, new_fname, new_lname, new_uname, new_password, new_birthday, new_comments, new_category):
        if new_password == "":
            self.sql("""UPDATE students SET first_name='%s', last_name='%s', username='%s', birthday='%s', comments='%s', category='%s'
                        WHERE first_name='%s' AND last_name='%s';""" % (self.duplicateSingleQuotes(new_fname), 
                                                                        self.duplicateSingleQuotes(new_lname),
                                                                        self.duplicateSingleQuotes(new_uname),
                                                                        self.duplicateSingleQuotes(new_birthday), 
                                                                        self.duplicateSingleQuotes(new_comments),
                                                                        self.duplicateSingleQuotes(new_category),
                                                                        self.duplicateSingleQuotes(old_fname), 
                                                                        self.duplicateSingleQuotes(old_lname)))
        else:
            new_password = hashlib.sha1(new_password).hexdigest()
            self.sql("""UPDATE students SET first_name='%s', last_name='%s', username='%s', password='%s', birthday='%s', comments='%s', category='%s'
                        WHERE first_name='%s' AND last_name='%s';""" % (self.duplicateSingleQuotes(new_fname), 
                                                                        self.duplicateSingleQuotes(new_lname),
                                                                        self.duplicateSingleQuotes(new_uname),
                                                                        self.duplicateSingleQuotes(new_password),
                                                                        self.duplicateSingleQuotes(new_birthday), 
                                                                        self.duplicateSingleQuotes(new_comments),
                                                                        self.duplicateSingleQuotes(new_category),
                                                                        self.duplicateSingleQuotes(old_fname), 
                                                                        self.duplicateSingleQuotes(old_lname)))   
                
    #Create a new student
    def addStudent(self, fname, lname, uname, password,birthday, comments, category):
        self.sql("""INSERT INTO students (first_name, last_name, username, password, birthday, comments, category) 
                    VALUES('%s','%s','%s','%s','%s','%s','%s')""" % (self.duplicateSingleQuotes(fname),
                                                     self.duplicateSingleQuotes(lname),
                                                     self.duplicateSingleQuotes(uname),
                                                     self.duplicateSingleQuotes(hashlib.sha1(password).hexdigest()),
                                                     self.duplicateSingleQuotes(birthday),
                                                     self.duplicateSingleQuotes(comments),
                                                     self.duplicateSingleQuotes(category)))

    #Do a login check. Returns a Student object
    def login(self, username, passwordHash):
	result = self.sql("""SELECT * FROM students 
                             WHERE username='%s' 
                             AND password='%s'""" % (username, passwordHash))
        if len(result) == 0:
	    return None
	elif len(result) != 1:
	    raise Exception
        else:
            return Student(result[0], Record(result[0]["student_id"], self))		
    #Method for preventing sql injection through singlequotes
    def duplicateSingleQuotes(self, string):
        new_string = ""
        for char in str(string):
            if char == "'":
                new_string+=char
                new_string+="'"
            else:
                new_string+=char

        return new_string
    
    #Convenience method
    def commit(self):
        self.db.commit()

#Class for creating widgets of a specfif style
class WidgetFactory(object):

    @staticmethod
    def makeButton(parent, text, command=None, width=15, height=0, fontSize=14):
        BG = "#99CCFF"
        hBG = "#55CCFF"
        FG = "#336677"
        hFG = "#55CCFF"
        
        return Button(parent,
                      text=text,
                      command=command,
                      width=width,
                      height=height,
                      bg=BG,
                      bd=3,
                      font=("Helvetica", fontSize, "bold"),
                      activebackground = hBG,
                      highlightbackground = FG,
		      highlightcolor = hFG,
		      highlightthickness = 4,
                      relief=FLAT)
    
    @staticmethod
    def makeEntry(parent, width=20, **kwargs):
        return Entry(parent,
                    width=width,
                    **kwargs)

    @staticmethod
    def makeLabel(parent, text, fontSize=12, fontStyle="normal"):
        BG = "#99CCFF"

        return Label(parent,
                     text=text,
                     bg=BG, 
                     font=("Helvetica", fontSize, fontStyle))
    @staticmethod
    def makeMessage(parent, text, width=200, fontSize=12, fontStyle="normal"):
        BG = "#99CCFF"

        return Message(parent,
                     text=text,
                     width=width,
                     bg=BG,
                     font=("Helvetica", fontSize, fontStyle))

    @staticmethod
    def makeOptionMenu(parent, string_variable, options, width=15, fontSize=12):
        BG = "#99CCFF"
        hBG = "#55CCFF"
        FG = "#336677"
        hFG = "#55CCFF"
    
        om = OptionMenu(parent,
                          string_variable,
                          *options)
        
        om.configure(width=width,
                     bg=BG,
                     bd=3,
                     font=("Helvetica", fontSize, "normal"),
                     activebackground = hBG,
                     highlightbackground = FG,
                     highlightcolor = hFG,
                     highlightthickness = 4,
                     relief=FLAT)
        return om
