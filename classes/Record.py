#!/usr/bin/env python

class Record(object):

    """stores best scores and times for each list that has been attempted, keyed by list_id"""
    listScores = {}
    """stores attempts and completion for attempted words, keyed by word_id"""
    wordCompletion = {}

    def __init__(self, student_id, dbconn):
        self.student_id = student_id
        scoresResults = dbconn.sql("""SELECT * FROM 
                                Records 
                                WHERE student_id='%d'""" % 
                                (student_id))

        for row in scoresResults:
            self.listScores[row["list_id"]] = (row["best_time"],
                                          row["high_score"],
                                          row["completion"])
        
        completionResults = dbconn.sql("""SELECT * FROM 
                                          wordcompletion
                                          WHERE student_id='%d'""" %
                                          (student_id))

        for row in completionResults:
            self.wordCompletion[row["word_id"]] = (row["completed"], row["attempts"])

    def getListScores(self, list_id=None):
        """Returns a specific score or the entire dict"""
        if list_id != None:
            return self.listScores[list_id]
        else:
            return self.listScores
    
    
    def getWordCompletion(self, word_id=None):
        """Returns a specific completion or the entire dict"""
        if word_id != None:
            return self.wordCompletion[word_id]
        else:
            return self.wordCompletion 
    
    def addScore(self, list_id, best_time, high_score, completion):
        self.listScores[list_id] =(best_time, high_score, completion)
    
    def addCompletion(self, word_id, completed, attempts):
        self.wordCompletion[word_id] = (completed, attempts)

    def commitRecord(self, dbconn):
        #Do a potentially really bad SQL query
        for key in self.listScores.keys():
            dbconn.sql("INSERT OR REPLACE INTO records (student_id, list_id, best_time, high_score, completion) VALUES ('%d', '%d', '%d', '%d', '%d')" % (self.student_id, key, self.listScores[key][0], self.listScores[key][1], self.listScores[key][2]))

        for key in self.wordCompletion.keys():
            dbconn.sql("INSERT OR REPLACE INTO wordcompletion (student_id, word_id, completed, attempts) VALUES ('%d', '%d', '%s', '%d')" % (self.student_id, key, self.wordCompletion[key][0], self.wordCompletion[key][1]))

        dbconn.commit()
