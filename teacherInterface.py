#!/usr/bin/env python
from Tkinter import Tk, Frame, Label, SUNKEN, W, LEFT, Entry, Message, Button
from Views import ListView, CreateView, StudentView
from Utils import SpellingDatabase
from functools import partial
import tkFont, hashlib

def main():
    def switch_frame(frameNumber, event):
        if frameNumber == 1:
            viewlists.pack_forget()
            viewcreate.pack()
            viewstudents.pack_forget()
            lNavViewLists.configure(bg="white", fg="black")
            lNavCreateLists.configure(bg="#DDDDDD", fg="#8800AA")
            lNavStudentRecords.configure(bg="white", fg="black")
            viewcreate.update_category()
        elif frameNumber == 2:
            viewcreate.pack_forget()
            viewlists.pack()
            viewstudents.pack_forget()
            lNavCreateLists.configure(bg="white", fg="black")
            lNavViewLists.configure(bg="#DDDDDD", fg="#8800AA")
            lNavStudentRecords.configure(bg="white", fg="black")
            viewlists.update()
        else:
            viewcreate.pack_forget()
            viewlists.pack_forget()
            viewstudents.pack()
            lNavCreateLists.configure(bg="white", fg="black")
            lNavViewLists.configure(bg="white", fg="black")
            lNavStudentRecords.configure(bg="#DDDDDD", fg="#8800AA")
    
    def login():
        def submit(event=None):
            password = hashlib.sha1(ePassword.get()).hexdigest()
            teacher_result = db.login('admin', password)
            if teacher_result == None:
                bSubmit.pack_forget()
                mError.pack()
                bSubmit.pack()
            else:
                login_root.destroy()
         
        db = SpellingDatabase()
        login_root = Tk()
        login_root.title("Spelling Aid")
        login_root.protocol("WM_DELETE_WINDOW", lambda: exit())
        fContainer = Frame(login_root)
        mMessage = Message(text="Enter the password to do admin actions", width=180)
        lPassword = Label(fContainer,text="Password:")
        ePassword = Entry(fContainer, show="*")
        ePassword.focus_force()
        mError = Message(fContainer, text="The details you entered were incorrect. Try Again", fg="red",width=200)
        bSubmit = Button(fContainer, text="Submit", command=submit)
        login_root.bind("<Return>", submit)
        
        mMessage.pack()
        lPassword.pack()
        ePassword.pack()
        bSubmit.pack(pady=5)
        fContainer.pack(pady=20, padx=20)
        
        login_root.mainloop()
    
    login()

    root = Tk()
    root.geometry('1004x610')
    border_width = 1
    border_style = SUNKEN
    background_colour = "#FFFFFF"
    default_height=600

    nav_frame = Frame(height=default_height,
                      width=200,
                      bd=border_width,
                      relief=border_style,
                      bg=background_colour)

    content_frame = Frame(height=default_height, 
                          width=804, 
                          bd=border_width,
                          relief=border_style)

    
    nav_frame.grid(column=0, row=0)
    content_frame.grid(column=1, row=0)
    nav_frame.grid_propagate(0)
    content_frame.pack_propagate(0)

    #Create fonts for navLabels
    fontMouseOver = tkFont.Font(family="Helvetica", size=14, underline=True)
    fontMouseOut = tkFont.Font(family="Helvetica", size=14, underline=False)

    #Creating Navigation Labels
    lNavStudentRecords = Label(nav_frame, 
                               text="Student Records", 
                               bg="white", 
                               font=fontMouseOut, 
                               bd=border_width, 
                               relief=border_style, 
                               width=20)
    lNavViewLists = Label(nav_frame, 
                          text="View Word Lists", 
                          bg="white", 
                          font=fontMouseOut, 
                          bd=border_width, 
                          relief=border_style, 
                          width=20)

    lNavCreateLists = Label(nav_frame, 
                            text="Create Word Lists", 
                            bg="white",
                            font=fontMouseOut, 
                            bd=border_width, 
                            relief=border_style, 
                            width=20)
    lNavLogout = Label(nav_frame,
                       text="Logout",
                       bg="white",
                       font=fontMouseOut,
                       bd=border_width,
                       relief=border_style,
                       width=20)
    
    #Binding Mouse events to the Labels
    #Mouse Clicks
    lNavViewLists.bind("<Button-1>", partial(switch_frame, 2))
    lNavCreateLists.bind("<Button-1>", partial(switch_frame, 1))
    lNavStudentRecords.bind("<Button-1>", partial(switch_frame, 3))
    lNavLogout.bind("<Button-1>", lambda(event): exit())
    #Mouse Movements
    lNavViewLists.bind("<Enter>", lambda(event):
            lNavViewLists.configure(font=fontMouseOver))
    lNavCreateLists.bind("<Enter>", lambda(event):
            lNavCreateLists.configure(font=fontMouseOver))
    lNavViewLists.bind("<Leave>", lambda(event):
            lNavViewLists.configure(font=fontMouseOut))
    lNavCreateLists.bind("<Leave>", lambda(event):
            lNavCreateLists.configure(font=fontMouseOut))
    lNavStudentRecords.bind("<Enter>", lambda(event):
            lNavStudentRecords.configure(font=fontMouseOver))
    lNavStudentRecords.bind("<Leave>", lambda(event):
            lNavStudentRecords.configure(font=fontMouseOut))
    lNavLogout.bind("<Enter>", lambda(event):
            lNavLogout.configure(font=fontMouseOver))
    lNavLogout.bind("<Leave>", lambda(event):
            lNavLogout.configure(font=fontMouseOut))
    #Gridding the labels
    lNavStudentRecords.grid(column=0, row=0)
    lNavViewLists.grid(column=0,row=1)
    lNavCreateLists.grid(column=0, row=2)
    lNavLogout.grid(column=0, row=3)
    
    #Creating the two views we have so far 
    viewcreate = CreateView(content_frame, default_height, 800, border_style, 
                              border_width, background_colour)

    viewlists = ListView(content_frame, default_height, 800, border_style, 
                              border_width, background_colour)

    viewstudents = StudentView(content_frame, 800, default_height)
    viewstudents.pack()
    
    root.mainloop()


if __name__=='__main__':
    main()
