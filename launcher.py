#!/usr/bin/env python
from Tkinter import Tk, Button
from Tkconstants import FLAT
import os
from Utils import WidgetFactory

def students(window):
    window.withdraw()
    os.system('python studentInterface.py')
    window.deiconify()

def teachers(window):
    window.withdraw()
    os.system('python teacherInterface.py')
    window.deiconify()

def main():
    root=Tk()
    root.geometry('300x200')
    root.title('Spelling Aid')
    root['bg'] = "#99CCFF"
    
    bStudent = WidgetFactory.makeButton(root, "Students")
    bStudent['command'] = lambda: students(root)
    
    bTeachers = WidgetFactory.makeButton(root, "Teachers")
    bTeachers['command'] = lambda: teachers(root)
    
    bQuit = WidgetFactory.makeButton(root, "Quit")
    bQuit['command'] = lambda: exit()

    bStudent.pack(padx=5, pady=10)
    bTeachers.pack(padx=5,pady=10)
    bQuit.pack(padx=5,pady=10)

    root.mainloop()

if __name__ == '__main__':
    main()
