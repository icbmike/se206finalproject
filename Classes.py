#!/usr/bin/env python
class Student:
    
    #The variable holding the Record object associated with the Student 
    record=None

    def __init__(self, (studentID, username, password, fname, lname, birthday, comments, category), record):
        """Student class is essentially an object representation of a record from the database.
        Thus it has fields that are identical to fields from the students table"""
        self.id = studentID
        self.username = username
        self.password = password
        self.fname = fname
        self.lname = lname
        self.birthday = birthday
        self.comments = comments
        self.record = record
        self.category = category
    
    def getInfo(self):
        """Return a dict containing the basic information about a student"""
        return {'id': self.id, 'username': self.username, 'passwordHash': self.password, 'fname': self.fname, 'lname':self.lname, 'birthday':self.birthday, 'comments':self.comments, 'category':self.category}


    """Set and get the record object"""
    def setRecord(self, record):
        self.record = record

    def getRecord(self):
        return self.record

class Record(object):
    #Stores best scores and times for each list that has been attempted, keyed by list_id"""
    listScores = {}
    #Stores attempts and completion for attempted words, keyed by word_id"""
    wordCompletion = {}

    def __init__(self, student_id, dbconn):
        #Store the student_id of the student that this record belongs to for convenience"""
        self.student_id = student_id
        #Like the Student class, the record class is really just an object representation of database information
        #However it stores 'multiple rows' of the two tables: Records and wordCompletion
        scoresResults = dbconn.sql("""SELECT * FROM 
                                Records 
                                WHERE student_id='%d'""" % 
                                (student_id))

        for row in scoresResults:
            self.listScores[row["list_id"]] = (row["best_time"],
                                          row["high_score"],
                                          row["completion"])
        
        completionResults = dbconn.sql("""SELECT * FROM 
                                          wordcompletion
                                          WHERE student_id='%d'""" %
                                          (student_id))

        for row in completionResults:
            self.wordCompletion[row["word_id"]] = (row["completed"], row["attempts"])


    def getListScores(self, list_id=None):
        """Returns a specific score or the entire dict"""
        if list_id != None:
            return self.listScores[list_id]
        else:
            return self.listScores
    
    
    def getWordCompletion(self, word_id=None):
        """Returns a specific completion or the entire dict"""
        if word_id != None:
            return self.wordCompletion[word_id]
        else:
            return self.wordCompletion 
    
    #Add a record for a list
    def addScore(self, list_id, best_time, high_score, completion):
        self.listScores[list_id] =(best_time, high_score, completion)

    #add completion information for a word
    def addCompletion(self, word_id, completed, attempts):
        self.wordCompletion[word_id] = (completed, attempts)
    
    #Clears all the rows from the object and the database
    def reset(self, db):
        self.listScores = {}
        self.wordCompletion = {}
        db.sql("""DELETE FROM wordCompletion 
                           WHERE student_id = '%s'""" % (self.student_id))
        db.sql("""DELETE FROM Records 
                           WHERE student_id = '%s'""" % (self.student_id))
        db.commit()


    #Add changes made to the object to the database  
    def commitRecord(self, dbconn):
        #Check if there is a row for this record already. If so, update it, if not insert it.
        for key in self.listScores.keys():
            record_check = dbconn.sql("SELECT student_id, list_id FROM records where student_id = '%s' AND list_id = '%s'" % (self.student_id, key))
            if len(record_check) == 0:
                dbconn.sql("INSERT INTO records (student_id, list_id, best_time, high_score, completion) VALUES ('%d', '%d', '%d', '%d', '%d')" % (self.student_id, key,self.listScores[key][0], self.listScores[key][1], self.listScores[key][2]))
            else:
                dbconn.sql("UPDATE records SET best_time='%s', high_score='%s', completion='%s' WHERE student_id='%s' AND list_id='%s'"%(self.listScores[key][0], self.listScores[key][1], self.listScores[key][2], self.student_id, key))

        for key in self.wordCompletion.keys():
            completion_check = dbconn.sql("SELECT student_id, word_id FROM wordcompletion WHERE student_id='%s' AND word_id='%s'" % (self.student_id, key))
            if len(completion_check) == 0:
                dbconn.sql("INSERT INTO wordcompletion (student_id, word_id, completed, attempts) VALUES ('%d', '%d', '%s', '%d')" % (self.student_id, key, self.wordCompletion[key][0], self.wordCompletion[key][1]))
            else:
                dbconn.sql("UPDATE wordcompletion SET completed='%s', attempts='%d' WHERE student_id='%d' AND word_id='%d'" % (self.wordCompletion[key][0], self.wordCompletion[key][1], self.student_id, key))

        dbconn.commit()

#Class that represents a list
class WordList(object):
    
    id=-1
    name= ""
    words = []
    source = ""
    dateEdited = ""
    numWords = -1
    category = ""
    
    #Stores a record from the lists table as an object
    def __init__(self, id, name, words, source, date_edited, num_words, category):
        self.id=id
        #words is an array of Words
        self.words=words
        self.source=source
        self.dateEdited=date_edited
        self.numWords = num_words
        self.current = 0
        self.category = category
        self.name = name

    #Standard getters and setters
    def getWords(self):
        return self.words
    
    def getName(self):
        return self.name
    def getCategory(self):
        return self.category
    def getID(self):
        return self.id
    
    def getDifficulty(self):
        return int(self.category[2])

    def getSource(self):
        return self.source
    
    def getDateEdited(self):
        return self.dateEdited
    
    def getNumWords(self):
        return self.numWords
    
    def getCurrentIndex(self):
        return self.current
    
    def setWords(self, words):
        self.words = words
    
    def setSource(self, source):
        self.source = source
    
    def setDateEdited(self, dateEdited):
        self.dateEdited = dateEdited
    
    def setNumWords(self, numWords):
        self.numWords = numWords
    
    def setID(self, id):
        self.id=id
    
    #These next two methods alow the WordList object to be iterated.
    #Returns items from the words array
    def __iter__(self):
        return self
    
    def next(self):
        if self.current >= len(self.words):
            raise StopIteration
        else:
            self.current += 1
            return self.words[self.current -1]

#Very basic holder class for a record from the words table
class Word(object):
    id = -1
    word = ""
    definition = ""
    usage=""
    difficulty=""

    def __init__(self, word, definition, usage, difficulty, id):
        self.id = id
        self.word = word
        self.definition = definition
        self.usage = usage
        self.difficulty = difficulty

    def getWord(self):
        return self.word

    def getID(self):
        return self.id
    
    def getDefinition(self):
        return self.definition
    
    def getUsage(self):
        return self.usage
    
    def getDifficulty(self):
        return self.difficulty
    
