The program can be run using the project1.sh script.
It starts the python script, launcher.py which lets you choose between the student and teacher apps, 
either of which could be run seperately by themselves.

Currently the student app requires you to login in while the teacher app does not.
You can use the teacher app to create a new profile to login to or modify someones account.
