#!/usr/bin/env python
from Tkinter import Tk, Button, Label, Entry,Frame, Message, Scrollbar, StringVar, TclError, PhotoImage, OptionMenu, Text
from Tkconstants import LEFT, RIGHT, TOP, CENTER, END, W, N, E, NW, SUNKEN, DISABLED, NORMAL,X,Y, BOTH
from Utils import SpellingDatabase, FestivalInterface,TldrParser, ListPane, WidgetFactory as WF
from datetime import date
import tkFileDialog, tkFont,time, random
from Classes import WordList, Word
class StudentView(Frame):
    def __init__(self, parent, width, height):
        #Call the initializer of the class we're inheriting from
        Frame.__init__(self, parent)

        self.db = SpellingDatabase()
        
        #Create the list frame
        student_list_frame = Frame(self)
        #Populate the frame 
        self.student_listpane = ListPane(student_list_frame, height=30, width = 35)
        self.student_listpane.singleClickFunc=self.display_details
        sort_options_list = ['First Name', 'Last Name']
        self.sort_v = StringVar()
        self.sort_v.set(sort_options_list[1])
        self.sort_option = OptionMenu(student_list_frame, self.sort_v, *sort_options_list, command=self.update_sort) 
        sort_label = Label(student_list_frame, text="Sort by:")
        title_label = Label(student_list_frame, text="Student Details")
        #Grid the components
        title_label.grid(row=0, column=0, sticky=W,pady=5)
        self.student_listpane.grid(row=1, column=0, columnspan=2)
        sort_label.grid(row=2, column=0, sticky=W, padx = 2)
        self.sort_option.grid(row=2, column=0, padx=50, sticky=W)
        
        #Create the details frame
        student_details_frame=Frame(self) 
        #populate the frame
        fname_label = Label(student_details_frame, text="First Name:")
        lname_label = Label(student_details_frame, text="Last Name:")
        uname_label = Label(student_details_frame, text="Username:")
        password_label = Label(student_details_frame, text="Set Password:")
        category_label = Label(student_details_frame, text="Category:")
        age_label = Label(student_details_frame, text="Birthday:")
        comments_label = Label(student_details_frame, text="Comments:")

        self.fname_entry = Entry(student_details_frame, width=30)
        self.lname_entry = Entry(student_details_frame, width=30)
        self.uname_entry = Entry(student_details_frame, width=30)
        self.password_entry = Entry(student_details_frame, width=30)
        self.category_options = ['Child', 'ESOL',  'Spelling Bee']
        self.category_var = StringVar(student_details_frame)
        self.category_var.set(self.category_options[0])
        category_combo = OptionMenu(student_details_frame, self.category_var, *self.category_options)
        self.age_entry = Entry(student_details_frame, width=30)
        self.comments_text = Text(student_details_frame, width=65, height=10)
        enable_edit_button = Button(student_details_frame, text="Edit Details", command=self.update_student)
        add_student_button = Button(student_details_frame, text="Add Student", command=self.insert_student)
        remove_student_button = Button(student_details_frame, text="Remove Student", command=self.remove_student)
        
        #Grid the components
        fname_label.grid(row=0, column=0, sticky=W, padx=3)
        lname_label.grid(row=1, column=0, sticky=W, padx=3)
        uname_label.grid(row=2, column=0, sticky=W, padx=3)
        password_label.grid(row=3,column=0, sticky=W, padx=3)
        category_label.grid(row=4, column=0, sticky=W, padx=3)
        age_label.grid(row=5, column=0, sticky=W, padx=3)
        comments_label.grid(row=6, column=0, sticky=W, padx=3)
        self.fname_entry.grid(row=0, column=1, sticky=W)
        self.lname_entry.grid(row=1, column=1, sticky=W)
        self.uname_entry.grid(row=2, column=1, sticky=W)
        self.password_entry.grid(row=3, column=1, sticky=W)
        category_combo.grid(row=4, column=1, sticky=W)
        self.age_entry.grid(row=5, column=1, sticky=W)
        self.comments_text.grid(row=7, column=0, columnspan=2, sticky=W, padx=10)
        enable_edit_button.grid(row=8, column=0, padx=7)
        add_student_button.grid(row=8, column=1, sticky=W)
        remove_student_button.grid(row=8, column=1, sticky=W, padx=110)
        
        #pack the two frames
        student_list_frame.pack(side=LEFT)
        student_details_frame.pack(side=LEFT)

        #Load student table from database and populate the Listpane
        student_records = self.db.sql("SELECT last_name, first_name From students WHERE username <> 'admin' ORDER By last_name")
        names_list = []
        for record in student_records:
            names_list.append("%s, %s" % (record[0], record[1]))
        
        self.student_listpane.display(names_list)
    
    #Redisplay the listpane with the appropriate order
    def update_sort(self, option):
        order_by=""
        if option == "First Name": order_by = 'first_name'
        else: order_by = 'last_name'
        
        student_records = self.db.sql("SELECT last_name, first_name From students WHERE username <> 'admin'ORDER By %s" % order_by)
        names_list = []
        for record in student_records:
            if option == "First Name": names_list.append("%s %s" % (record[1], record[0]))
            else: names_list.append("%s, %s" % (record[0], record[1]))
        self.student_listpane.display(names_list)
    
    #Display the details of the student that was clicked on in the listpane
    def display_details(self, item_index):
        self.selected_student = item_index
        first_name = ""
        last_name = ""
        if self.sort_v.get() == "First Name":
            first_name, last_name = self.student_listpane.get(item_index).split(' ')
        else:
            last_name, first_name = self.student_listpane.get(item_index).split(', ')
        student_record = self.db.sql("SELECT * from students where first_name = '%s' and last_name = '%s'" % (first_name, last_name))[0]
        #Clear the current contents and insert the retrieved details
        self.fname_entry.delete(0, END)
        self.fname_entry.insert(0, first_name)
        self.lname_entry.delete(0, END)
        self.lname_entry.insert(0, last_name)
        self.uname_entry.delete(0, END)
        self.uname_entry.insert(0, student_record['username'])
        if student_record['category'] == 'Child':
            self.category_var.set(self.category_options[0])
        elif student_record['category'] == 'ESOL':
            self.category_var.set(self.category_options[1])
        else:
            self.category_var.set(self.category_options[2])
            
        self.age_entry.delete(0, END)
        self.password_entry.delete(0,END)
        self.age_entry.insert(0, student_record['birthday'])
        self.comments_text.delete(1.0, END)
        self.comments_text.insert(1.0, student_record['comments'])
    
    #Delete the current selected student
    def remove_student(self):
        if self.fname_entry.get() == "" and self.lname_entry.get() == "":
            return
        first_name = ""
        last_name = ""
        if len(self.student_listpane.get(self.selected_student))==0: return
        if self.sort_v.get() == "First Name":
            first_name, last_name = self.student_listpane.get(self.selected_student).split(' ')
        else:
            last_name, first_name = self.student_listpane.get(self.selected_student).split(', ')
        self.db.sql("DELETE FROM students where first_name = '%s' and last_name = '%s'" % (first_name, last_name))
        self.db.commit()
        self.update_sort(self.sort_v.get())
    
    #Update te curerntly selected student
    def update_student(self):
        if len(self.student_listpane.getDisplayed()) == 0: return
        if self.fname_entry.get() == "" and self.lname_entry.get() == "": return

        old_first_name = ""
        old_last_name = ""
        if self.sort_v.get() == "First Name":
            old_first_name, old_last_name = self.student_listpane.get(self.selected_student).split(' ')
        else:
            old_last_name, old_first_name = self.student_listpane.get(self.selected_student).split(', ')
        
        self.db.update_student(old_first_name, old_last_name, self.fname_entry.get(), self.lname_entry.get(), self.uname_entry.get(), self.password_entry.get(), self.age_entry.get(), self.comments_text.get(1.0,END), self.category_var.get())
        self.db.commit()
        self.update_sort(self.sort_v.get())
    
    #Create a new student  
    def insert_student(self):
        current_students = self.student_listpane.getDisplayed()
        if self.fname_entry.get() == "" and self.lname_entry.get() == "":
            return
        for student in current_students:
            first_name = ""
            last_name = ""
            if self.sort_v.get() == "First Name":
                first_name, last_name = student.split(' ')
            else:
                last_name, first_name = student.split(', ')
           
            if first_name == self.fname_entry.get() and last_name == self.lname_entry.get():
                return

        self.db.addStudent(self.fname_entry.get(), self.lname_entry.get(), self.uname_entry.get(), self.password_entry.get(), self.age_entry.get(), self.comments_text.get(1.0, END), self.category_var.get())
        self.db.commit()
        self.update_sort(self.sort_v.get())

class Tester(object):
    #The interface for testing.
    def __init__(self, wordList, student, mainWindow, db):
        self.mainWindow=mainWindow
        self.wordList = wordList
        self.student = student
        self.studentRecord = student.getRecord()
        self.current_word = wordList.next()
        self.festival = FestivalInterface()
        self.elapsedTime = 0
        self.keybouncetime = time.time()
        self.wordCompletion = {}
        self.numCompleted = 0
        self.stopTiming = False
        self.after_id = None
        self.db = db
        
        #Create window
        self.root = Tk()
        self.root.title('Spelling Test')
        self.root['bg'] = "#99CCFF"
        self.root.option_add("*Label.Font", "helvetica 12 bold")
        self.root.option_add("*Message.Font", "helvetica 11 normal")
        self.root.option_add("*Button.Font", "helvetica 11 normal")
        self.root.protocol("WM_DELETE_WINDOW", self.cancel)
        
        #Create the opening frame
        self.openingFrame()
        self.root.mainloop()
    
    #the method for when we are leaving the testing interface. Updates the database and the student record
    def cancel(self):     
        #Destroy window
        if self.after_id is not None:
            self.root.after_cancel(self.after_id)
        self.root.quit()
        self.root.destroy()
        try:
            self.current_word = self.wordList.next()
            self.mainWindow.deiconify()
            return
        except StopIteration:
            pass
        #Commit any completion
        self.calculateStats()
        #Check if the time elapsed is a better time
        if self.wordList.getID() in self.studentRecord.getListScores():
            #Check if calculated score is better than stored score
            if self.studentRecord.getListScores(self.wordList.getID())[1] > self.score:
                self.score = self.studentRecord.getListScores(self.wordList.getID())[1]
            #Check if stored completion is better
            if self.studentRecord.getListScores(self.wordList.getID())[2] > self.completion:
                self.completion = self.studentRecord.getListScores(self.wordList.getID())[2]
            #Check if stored time is better and only apply better time if highscore is better also
            if self.studentRecord.getListScores(self.wordList.getID())[0] < self.elapsedTime and self.score == self.studentRecord.getListScores(self.wordList.getID())[1]:
                self.elapsedTime = self.studentRecord.getListScores(self.wordList.getID())[0]    
        #Update the contents of the Record
        if self.wordList.getID() != -1:
            self.studentRecord.addScore(self.wordList.getID(), self.elapsedTime, self.score, self.completion)
        self.studentRecord.commitRecord(self.db)
        self.student.setRecord(self.studentRecord)
        self.mainWindow.deiconify()

    def calculateStats(self):
        self.completion = float(self.numCompleted)/float(self.wordList.getNumWords()) * 100
        self.score = (self.completion * 10 - (self.elapsedTime/self.wordList.getNumWords() - 2.5)*2.5)*(1+ (self.wordList.getDifficulty()-1)*0.25)

    def displayResults(self):
        #Close up the test view and do calculations
        self.fMainFrame.pack_forget()
        self.stopTiming = True
        self.fMainFrame.destroy()
        self.root.after_cancel(self.after_id)
        self.calculateStats()

        if self.wordList.getID() in self.studentRecord.getListScores():
            #Check if stored time is better
            storedTime = self.studentRecord.getListScores(self.wordList.getID())[0] 
            #Check if calculated score is better than stored score
            storedScore = self.studentRecord.getListScores(self.wordList.getID())[1]
            #Check if stored completion is better
            storedCompletion = self.studentRecord.getListScores(self.wordList.getID())[2]
        else:
            storedTime = float("inf")
            storedScore = 0
            storedCompletion = 0
        #Calculate the differences
        diffScore = self.score - storedScore
        diffCompletion = self.completion - storedCompletion
        diffTime = self.elapsedTime - storedTime
        #Create the widgets for the results view
        fResultsFrame = Frame(self.root, bg="#99CCFF")
        lTestResults = WF.makeLabel(fResultsFrame,"Test Results")
        lListName = WF.makeLabel(fResultsFrame,self.wordList.getName())
        lListCategory = WF.makeLabel(fResultsFrame,self.wordList.getCategory())
        bDone = WF.makeButton(fResultsFrame, "Done", command=self.cancel)
        lLabelScore = WF.makeLabel(self.root, "Score: ")
        lLabelCompletion = WF.makeLabel(self.root, "Completion(%): ")
        lLabelTime = WF.makeLabel(self.root, "Time(s): ")
        #Create a separate frame for the actual results
        fResultsTable = Frame(fResultsFrame, bg="#99CCFF")
        lLabelScore = WF.makeLabel(fResultsTable,"Score: ")
        lLabelCompletion = WF.makeLabel(fResultsTable,"Completion(%): ")
        lLabelTime = WF.makeLabel(fResultsTable,"Time(s): ")
        lThis = WF.makeLabel(fResultsTable, "This Time")
        lLast = WF.makeLabel(fResultsTable, "Current Best")
        lThisScore = WF.makeLabel(fResultsTable, int(self.score))
        lLastScore = WF.makeLabel(fResultsTable, int(storedScore))
        lThisCompletion = WF.makeLabel(fResultsTable, int(self.completion)) 
        lLastCompletion = WF.makeLabel(fResultsTable, int(storedCompletion))
        esecs = self.elapsedTime % 60
        emins = self.elapsedTime / 60
        lThisTime = WF.makeLabel(fResultsTable, "%02d:%02d" % (emins,esecs))
        #ormat the time appropriately
        if storedTime == float("inf"):
            lLastTime = WF.makeLabel(fResultsTable, "--:--")
        else:
            secs = storedTime % 60
            mins = storedTime / 60
            lLastTime = WF.makeLabel(fResultsTable, "%02d:%02d" % (mins, secs))
        lDiffScore = WF.makeLabel(fResultsTable,"")
        lDiffCompletion = WF.makeLabel(fResultsTable, "")
        lDiffTime = WF.makeLabel(fResultsTable, "")
        #Configure the diff labels based on your performance
        if diffScore > 0:
            lDiffScore.configure(fg="#00AA00", text="+%d" % (diffScore))
        elif diffScore == 0:
            lDiffScore.configure(fg="black", text=str(diffScore))
        else:
            lDiffScore.configure(fg="red", text="-%s" %(diffScore))
        
        if diffCompletion > 0:
            lDiffCompletion.configure(fg="#00AA00", text="+%d" % (diffCompletion))
        elif diffCompletion == 0:
            lDiffCompletion.configure(fg="black", text=str(diffCompletion))
        else:
            lDiffCompletion.configure(fg="red", text="-%s" %(diffCompletion))
        
        if diffTime < 0:
            if diffTime == -float("inf"):
                lDiffTime.configure(fg="#00AA00", text="--:--")
            else:
                lDiffTime.configure(fg="#00AA00", text="%d" % (diffTime))
        elif diffTime == 0:
            lDiffTime.configure(fg="black", text=str(diffTime))
        else:
            lDiffTime.configure(fg="red", text="+%s" %(diffTime))
        #Grid the results table
        lThis.grid(row=0,column=1)
        lLast.grid(row=0, column=3)
        lLabelScore.grid(row=1, column=0, sticky=W)
        lThisScore.grid(row=1, column=1)
        lDiffScore.grid(row=1, column=2)
        lLastScore.grid(row=1, column=3)
        lLabelCompletion.grid(row=2, column=0, sticky=W)
        lThisCompletion.grid(row=2, column=1)
        lDiffCompletion.grid(row=2, column=2)
        lLastCompletion.grid(row=2, column=3)
        lLabelTime.grid(row=3, column=0, sticky=W)
        lThisTime.grid(row=3, column=1)
        lDiffTime.grid(row=3, column=2)
        lLastTime.grid(row=3, column=3)
        
        #Grid the main widgets
        lTestResults.grid(row=0, column=0, columnspan=2)
        lListName.grid(row=1, column=0)
        lListCategory.grid(row=1, column=1)
        fResultsTable.grid(row=3, column=0, columnspan=2, padx=5, pady=3)
        bDone.grid(row=4, column=0, columnspan=2)
        fResultsFrame.pack()

    def submit(self, event=None):
        #Need to prevent "Key bouncing" so that festival doesn't freak out
        #Unfortunately even with a 2 second time limit enough information 
        #can be sent to festival to make it crash.
        #However this only happens when the Enter key is held down.
        now = time.time()
        if (now - self.keybouncetime) < 2:
            return
        self.keybouncetime = now
        #Only update the record for a word if we havent already completed it
        if self.current_word.getID() in self.studentRecord.getWordCompletion():
            if str(self.studentRecord.getWordCompletion()[self.current_word.getID()]) == "True":
                attempts = self.studentRecord.getWordCompletion(self.current_word.getID())[1]
                dont_update = True
            else:
                attempts = self.studentRecord.getWordCompletion(self.current_word.getID())[1] + 1
                dont_update=False
        else:
            attempts = 1
            dont_update=False
        #Check if the word entered was correct
        wordAttempt = self.eWordEntry.get()
        if wordAttempt == self.current_word.getWord():
            self.eWordEntry.configure(bg="green")
            self.lMisspeltWord.configure(text="")
            self.numCompleted += 1
            #Modify the record for the attempted word
            self.studentRecord.addCompletion(self.current_word.getID(),
                                             True,
                                             attempts)
        else:
            self.eWordEntry.configure(bg="red")
            self.lMisspeltWord.configure(text="Correct spelling: '%s'"%(self.current_word.getWord()))
            #Store completion in record
            self.studentRecord.addCompletion(self.current_word.getID(),
                                             True if dont_update else False, 
                                             attempts) 
        #Increment through wordList
        try:
            self.current_word = self.wordList.next()
            self.lProgress.configure(text="%d/%d" %(self.wordList.getCurrentIndex(), self.wordList.getNumWords()))
            self.eWordEntry.delete(0,END)
            self.eWordEntry.focus_force()
            self.speak()
        except StopIteration:
            self.displayResults()
        
    #Speak the current word using festival
    def speak(self):
        self.festival.speech(self.current_word.getWord())

    def mainFrame(self):
        #Destroy the opening frame
        self.fInitialFrame.destroy()
        
        """Creating the main frame and its widgets. 
        Bind the return key on the entry to the same action as the submit Button"""
        self.fMainFrame = Frame(self.root, bg="#99CCFF")
        self.lTime = WF.makeLabel(self.fMainFrame, "Time Elapsed: 00:00")
        self.lProgress = WF.makeLabel(self.fMainFrame, "%d/%d" % (self.wordList.getCurrentIndex(), self.wordList.getNumWords()))
        self.lMisspeltWord = WF.makeLabel(self.fMainFrame, "")
        photo = PhotoImage(master=self.fMainFrame, file="speaker.gif") 
        bSpeak = Button(self.fMainFrame,image=photo,width=60, height=60, command=self.speak, bg="#99CCFF")
        bSpeak.photo = photo
        self.eWordEntry = Entry(self.fMainFrame,width=30)
        self.eWordEntry.bind("<Return>", self.submit)
        bSubmit = WF.makeButton(self.fMainFrame, "Check!", command=self.submit,width=10)
        bSkip = WF.makeButton(self.fMainFrame, "I Don't Know", command=self.submit,width=10)
        bReturn = WF.makeButton(self.fMainFrame, "Exit", command=self.cancel,width=10)
        #Grid the elements
        self.lTime.grid(row=0, column=1)
        self.lProgress.grid(row=0, column=2)
        self.lMisspeltWord.grid(row=0, column=0, pady=3)
        bSpeak.grid(row=1, column=0, rowspan=2)
        self.eWordEntry.grid(row=1, column=1, padx=7)
        bSubmit.grid(row=1,column=2)
        bSkip.grid(row=2,column=2, pady=3)
        bReturn.grid(row=2, column=1)
        self.fMainFrame.pack(padx=10,pady=7)
        #Speak the first Word
        self.speak()
        #Schedule the timeupdate
        self.after_id = self.root.after(1000, self.timeIncr)
        self.eWordEntry.focus_force() 
    def timeIncr(self):
        self.elapsedTime +=1
        minutes = self.elapsedTime / 60
        seconds = self.elapsedTime % 60
        try:
            self.lTime.configure(text="Time Elapsed: %02d:%02d" % (minutes, seconds))
        except TclError:
            pass
        if not self.stopTiming:
            self.root.after(1000, self.timeIncr)

    def openingFrame(self):
        """Creating the opening frame. Will have two Buttons,
        one to return to the previous screen and the other to begin the test."""
        self.fInitialFrame = Frame(self.root, bg="#99CCFF")
        mMessage = WF.makeMessage(self.fInitialFrame, 
                "The test is timed for scoring purposes. Press the Start button to begin when ready.", width=250)
        fButtonFrame = Frame(self.fInitialFrame, bg="#99CCFF")
        bStart = WF.makeButton(fButtonFrame, "Start",width=10, command=lambda: self.mainFrame())
        bReturn = WF.makeButton(fButtonFrame, "Back", width=10, command=self.cancel)
        #Pack the elements and the frame
        mMessage.pack(pady=3)
        bStart.pack(side=LEFT,padx=3)
        bReturn.pack(side=LEFT)
        fButtonFrame.pack()
        self.fInitialFrame.pack(padx=20, pady=10)

#the Listview for the teacher interface
class ListView(Frame):
    def __init__(self, parent, height, width, border_style, border_width, background_colour):
        #Call Frams's __init__
        Frame.__init__(self, parent) 
        #create the frame that holds the widgets on the left
        lists_frame = Frame(self)
        #Add custom ListPane widget
        self.lists_list_pane = ListPane(lists_frame, height = 25)
        self.lists_list_pane.singleClickFunc = self.displayWords
        #Add labels and Buttons
        lists_label = Label(lists_frame, text="Word Lists")
        import_button = Button(lists_frame, text="Import", command=self.importList)
        export_button = Button(lists_frame, text="Export", command=self.exportList)
        #Grid everything
        lists_label.grid(row=0, column=0, sticky=W, pady=3, padx=2)
        self.lists_list_pane.grid(row=1, column=0, columnspan=2)
        import_button.grid(row=2, column=0, sticky=W, padx=2)
        export_button.grid(row=2, column=0) 
        
        #create the container frame that holds the widgets on the right
        words_frame = Frame(self)
        #create custom ListPane widget
        self.words_list_pane = ListPane(words_frame,height = 25)
        self.words_list_pane.singleClickFunc = self.displayWordInfo
        #create labels and buttons
        self.speak_word_button = Button(words_frame, text="Speak Word", command=self.speakWord, state=DISABLED)
        self.speak_list_button = Button(words_frame,text="Speak List", command=self.speakList, state=DISABLED)
        stop_speech_button = Button(words_frame,text="Stop Speech", command=self.stopSpeech)
        words_label = Label(words_frame, text="Words in List")
        
        #grid everything
        words_label.grid(row=0, column=0, sticky=W, pady=3, padx=2)
        self.words_list_pane.grid(row=1, column=0, columnspan=3)
        self.speak_word_button.grid(row=2, column=0)
        self.speak_list_button.grid(row=2, column=1)
        stop_speech_button.grid(row=2, column=2)
        
        #Create the infoFrame
        info_frame = Frame(self, height=140, width=width, bd=border_width, bg=background_colour, relief=border_style)

        self.info_text = Text(info_frame, state=DISABLED, font=tkFont.Font(family="Tahoma", size=9))
        self.info_text.pack(fill=BOTH)
        
        #grid the frames
        lists_frame.grid(column=0, row=0)
        words_frame.grid(column=1, row=0)
        info_frame.grid(column=0, row=1, columnspan=2)
        info_frame.pack_propagate(0)
        
        #Create databse and Festival connections
        self.db = SpellingDatabase()
        self.fest = FestivalInterface()

        list_records = self.db.getLists()
        list_names = []
        for row in list_records:
            list_names.append(row[1])
        self.lists_list_pane.display(list_names)
        self.words_list_pane.display(['...'])

    #Import a tldr file
    def importList(self):
        filename = tkFileDialog.askopenfilename(filetypes = (("Tldr Files", "*.tldr"),))
        if str(filename) == "":
            return
        tldr_parse = TldrParser(filename)
        list_name = filename[:-5].split('/')[-1]
        if self.db.importList(list_name, tldr_parse.getWordList(), tldr_parse.getSource(), tldr_parse.getDateEdited(), tldr_parse.getListSize()):
            self.lists_list_pane.insert(list_name)
        else:
            print "List with that name already exists!!!"
        self.db.commit()

    #Export a list to a tldr file
    def exportList(self):
        selected = self.lists_list_pane.get()
        filename = tkFileDialog.asksaveasfilename(filetypes = (("Tldr Files", "*.tldr"),), initialfile = selected)
        if str(filename) == "":
            return
        export_list = self.db.getList(selected)[0]
        word_list = self.db.getWords(selected)
        word_dict = {}
        for word in word_list:
            word = word[0]
            word_record = self.db.getWord(word)[0]
            word_dict[word] = [word_record[2],word_record[3],word_record[4]]
        tldr_parse = TldrParser()
        tldr_parse.setWordList(word_dict)
        tldr_parse.setSource(export_list[2])
        tldr_parse.setDateEdited(str(date.today()))
        tldr_parse.setListSize(export_list[4])
        tldr_parse.writetldr(filename)

    # Speak the selected word
    def speakWord(self):
        word = self.words_list_pane.get()
        self.fest.speech(word)
    
    #Speak the selected word list
    def speakList(self):
        word_list = self.words_list_pane.getDisplayed()
        long_sentence = ""
        for word in word_list:
            long_sentence += " %s." % (word)
        self.fest.speech(long_sentence)
    
    #Terminate any speech
    def stopSpeech(self):
        self.fest.resetSpeech()
    #Display the words from the selected list and display its meta data 
    def displayWords(self, item_index):
        word_records = self.db.getWords(self.lists_list_pane.get(item_index))
        word_names = []
        for row in word_records:
            word_names.append(str(row[0]))
        word_names.sort(key=str.lower)
        self.words_list_pane.display(word_names)
        
        list_record = self.db.getList(self.lists_list_pane.get(item_index))
        if len(list_record) > 0:
            list_record = list_record[0]
            info_string = """List: %s

Source: %s
Date Edited: %s
Number of Words in List: %s""" % (list_record[1], list_record[2], list_record[3], list_record[4])
            self.info_text.configure(state=NORMAL)
            self.info_text.delete(1.0, END)
            self.info_text.insert(1.0, info_string)
            self.info_text.configure(state=DISABLED)

            self.speak_word_button.configure(state=DISABLED)
            self.speak_list_button.configure(state=NORMAL)
        else:
            self.info_text.configure(state=NORMAL)
            self.info_text.delete(1.0, END)
            self.info_text.insert(1.0, "There are no lists in the database.\nImport one or create one")
            self.info_text.configure(state=DISABLED)
    #Display meta information for the selected word
    def displayWordInfo(self, item_index): 
        word_record = self.db.getWord(self.words_list_pane.get(item_index))
        if len(word_record) > 0:
            word_record = word_record[0]
            info_string = """Word: %s

Definition: %s
Usage: "%s"
Difficulty: %s""" % (word_record[1], word_record[2], word_record[3], word_record[4])
            self.info_text.configure(state=NORMAL)
            self.info_text.delete(1.0, END)
            self.info_text.insert(1.0, info_string)
            self.info_text.configure(state=DISABLED)
            
            self.speak_word_button.configure(state=NORMAL)
    
    #Update the list pane
    def update(self):        
        list_records = self.db.getLists()
        list_names = []
        for row in list_records:
            list_names.append(row[1])
        self.lists_list_pane.display(list_names)
        self.words_list_pane.display(['...'])

class PractiseView(Frame):
    def __init__(self, parent, student):
        self.student = student
        Frame.__init__(self, parent, bg="#99CCFF")
        self.db = SpellingDatabase()
        self.initGUI()
    
    def initGUI(self):
        def displayInfoAll(index):
                selectedList = self.db.getList(lpAll.get(index))[0]
                mListInfo.configure(text="List Name: %s\nSource: %s\nDate Edited: %s\nSize: %s words\nCategory: %s"%(selectedList['list_name'], selectedList['source'], selectedList['date_edited'], selectedList['num_words'], selectedList['category']))
            
        def displayInfoRecommended(index):
                selectedList = self.db.getList(lpRecommended.get(index))[0]
                mListInfo.configure(text="List Name: %s\nSource: %s\nDate Edited: %s\nSize: %s words\nCategory: %s"%(selectedList['list_name'], selectedList['source'], selectedList['date_edited'], selectedList['num_words'], selectedList['category']))
        
        def practiseSelected(event=None):
            #Get whats selected
            allSelection = lpAll.get()
            recommendedSelection = lpRecommended.get()
            #Make sure that something atually is selected and pass the element to a listTest()
            if allSelection is not None:
                self.listTest(allSelection)
            elif recommendedSelection is not None:
                self.listTest(recommendedSelection)
            else: 
                return
        
        #Create the select list view frame
        self.option_add("*OptionMenu.Font", "helvetica 12")
        #create left and right frames
        fLeft = Frame(self, bg="#99CCFF")
        fRight = Frame(self, bg="#99CCFF")
        #Create the ListPanes for selecting lists
        lRecommended = WF.makeLabel(fLeft, text="Recommended Lists:")
        lpRecommended = ListPane(fLeft, height=10, width=20)
        lpRecommended.singleClickFunc = displayInfoRecommended
        lAll = WF.makeLabel(fLeft, text="All Lists:")
        lpAll = ListPane(fLeft, height=10, width=20)
        lpAll.singleClickFunc = displayInfoAll
        #Populate the list panes
        allListResults = self.db.getLists()
        allListNames = []
        for row in allListResults:
            allListNames.append(row['list_name'])
        lpAll.display(allListNames)
        if self.student.getInfo()['category'] == "Child":
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'CL%'")
        elif self.student.getInfo()['category'] == "ESOL":
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'AL%'")
        else:
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'SB%'")
        recommendedListNames = []
        for row in recommendedListResults:
            recommendedListNames.append(row['list_name'])
        lpRecommended.display(recommendedListNames)
        #pack the left hand widgets
        lRecommended.pack()
        lpRecommended.pack()
        lAll.pack()
        lpAll.pack()
        #Create the right hand widgets
        mListInfo = WF.makeMessage(fRight, width=300, text="Select a list")
        bDoSelected = WF.makeButton(fRight, "Practise List!", command=practiseSelected)
        mRandom = WF.makeMessage(fRight, width=3000, text="Practise a random up to 30 word list from the selected category")
        fComboButtonContainer = Frame(fRight, bg="#99CCFF")
        category_options = ['AL1', 'AL2', 'CL1', 'CL2', 'CL3', 'CL4', 'CL5', 'CL6', 'CL7', 'CL8', 'SB1', 'UL1']
        self.category_variable = StringVar(fComboButtonContainer)
        self.category_variable.set(category_options[0])
        cbCategory = WF.makeOptionMenu(fComboButtonContainer, self.category_variable, category_options, width=5)
        bRand = WF.makeButton(fComboButtonContainer, "Practise Random List", command=self.randList, width=20)
        #pack the right hand widgets
        cbCategory.pack(side=RIGHT)
        bRand.pack(side=RIGHT, padx=5)
        mListInfo.pack(anchor=NW)
        bDoSelected.pack()
        mRandom.pack()
        fComboButtonContainer.pack()
        #Pack widgets
        fLeft.pack(side=LEFT, padx=5, pady=3)
        fRight.pack(side=LEFT, pady=3, fill=Y)
            
    def randList(self):
        self.listTest("", rand=True, category=self.category_variable.get())
    
    #Create an instance of the Tester interface 
    def listTest(self, listName, rand=False, category=""):
        
        if rand == True:
            wordResult = self.db.sql("SELECT * FROM words WHERE difficulty = '%s'" % (category)) 
            if len(wordResult) > 30:
                random.shuffle(wordResult)
                wordResult = wordResult[:30]
            
            words = []
            for row in wordResult:
                words.append(Word(row["word"], row["definition"], row["usage"], row["difficulty"], row["word_id"]))
            
            wordList = WordList(-1, "30 Random %s Words" % (category), words, "Random", "Right Now", len(wordResult), category)
        else:
            listResult = self.db.getList(listName)[0]
            wordResult = self.db.getWords(listName)
            words = []
            for row in wordResult:
                words.append(Word(row["word"], row["definition"], row["usage"], row["difficulty"], row["word_id"]))

            wordList = WordList(listResult["list_id"], listResult["list_name"],words, listResult["source"], listResult["date_edited"], listResult["num_words"], listResult["category"])
        
        self.winfo_toplevel().withdraw()
        Tester(wordList, self.student, self.winfo_toplevel(), self.db)

class CreateView(Frame):
    """A class describing the list creation page of the UI"""

    def __init__(self, parent, height, width, border_style, border_width,
                 background_colour):

        Frame.__init__(self, parent, height=height, width=width)

        #Connect to the word/list database 
        self.db = SpellingDatabase()
        self.lastSubmittedCategory = ""
        self.newListCategory=""
        #Create a frame containing a menu for choosing word category
        categorymenu_frame = Frame(self, width=340, height=30, relief=SUNKEN, bd=1)
        difficultymenu_frame = Frame(self, width=340, height=30, relief=SUNKEN, bd=1)
        categorymenu_frame.pack_propagate(0)
        categorymenu_label = Label(categorymenu_frame, text="Category:")
        difficulty_menu_label = Label(difficultymenu_frame, text="Difficulty:")
        wordlist_title = Label(categorymenu_frame, text="Select Words")
        #Menu options: one for each category of word
        categoryOptionList = ("Child", "ESOL", "Spelling Bee", "Custom")
        difficultyOptionList = range(1,9)
        self.category_v = StringVar()
        self.category_v.set(categoryOptionList[0])
        self.browse_difficulty_v = StringVar()
        self.browse_difficulty_v.set(difficultyOptionList[0])
        #Command causes word browser to be populated with words from chosen category
        self.category_menu = OptionMenu(categorymenu_frame, self.category_v, *categoryOptionList, command=self.update_category)
        self.difficulty_menu = OptionMenu(difficultymenu_frame, self.browse_difficulty_v, *difficultyOptionList, command=self.update_difficulty)
        self.category_menu.config(width=10)
        self.category_menu.pack(side=RIGHT)
        categorymenu_label.pack(side=RIGHT)
        self.difficulty_menu.pack(side=RIGHT)
        difficulty_menu_label.pack(side=RIGHT)
        wordlist_title.pack(side=LEFT)
        
        #Create frame for the title of the user list panel
        userlist_title_frame = Frame(self, width=340, height=30)
        userlist_title_frame.pack_propagate(0)
        userlist_title = Label(userlist_title_frame, text="Your New List")
        userlist_title_frame.grid(column=2, row=0)
        userlist_title.pack(side=LEFT)

        #Create frame for middle bar containing transfer buttons
	middlebar_frame = Frame(self, width = 70, height=400)
        middlebar_frame.grid_propagate(0)
        #Buttons transfer words from one list to the other
        transfer_right_button = Button(middlebar_frame, text="->", command=self.transfer_right)
        transfer_left_button = Button(middlebar_frame, text="<-", command=self.transfer_left)
        #Press this button to generate a random list from the current category
        random_list_button = Button(middlebar_frame, text="Create", command=self.random_list)
        random_list_label = Label(middlebar_frame, text="Random\nList")
        self.random_list_entry = Entry(middlebar_frame, width=3, justify=RIGHT)
        self.random_list_entry.insert(0, 15) 
	transfer_left_button.grid(column=0, row=1, padx=15, pady=50)
        transfer_right_button.grid(column=0, row=0, padx=15, pady=50)
        random_list_label.grid(column=0, row=2, pady=3)
        self.random_list_entry.grid(column=0, row=3, pady=3)
        random_list_button.grid(column=0, row=4, pady=3)
        middlebar_frame.grid(column=1, row=1)
        #random_list_button.grid(column=0, row=2)

        #Create frame for "Add New Word" menu
        addword_frame = Frame(self, width=750, height=100, bd=2, relief=SUNKEN)
        addword_frame.grid_propagate(0)
        addword_label = Label(addword_frame, text = "Add a new word:")
        word_label = Label(addword_frame, text="Word:")
        def_label = Label(addword_frame, text="Definition:")
        use_label = Label(addword_frame, text="Example of Use:")
        difficulty_label = Label(addword_frame, text="Difficulty:")
        #Entry boxes and an option menu allowing the user to enter attributes of the new word
        self.addword_entry = Entry(addword_frame, width = 35)
        self.adddefinition_entry = Entry(addword_frame, width=35)
        self.adduse_entry = Entry(addword_frame, width=35)
        self.difficulty_v = StringVar()
        self.difficulty_v.set("1")
        difficulty_list = range(1,9)
        self.difficulty_menu = OptionMenu(addword_frame, self.difficulty_v, *difficulty_list)
        #Pressing this button adds the new word to the database
        addword_button = Button(addword_frame, text = "Add", command = self.add_word)
        addword_label.grid(row=0, column=0, sticky=W)
        addword_button.grid(row=0, column=3, pady=2, sticky=E)
        word_label.grid(row=1, column=0, sticky=W)
        def_label.grid(row=1, column=2, sticky=E)
        use_label.grid(row=2, column=0, sticky=W)
        difficulty_label.grid(row=2, column=2, sticky=E)
        self.addword_entry.grid(row=1, column=1, pady=2, sticky=E)
        self.adddefinition_entry.grid(row=1, column=3, pady=2, sticky=E)
        self.adduse_entry.grid(row=2, column=1, pady=2, sticky=E)
        self.difficulty_menu.grid(row=2, column=3, pady=2, sticky=E)
        addword_frame.grid(column=0, columnspan=3,row=3)
        
        #Frame for menu allowing users to save their new lists
        savelist_frame = Frame(self, width=340, height=30, bd=2, relief=SUNKEN)
        savelist_frame.pack_propagate(0)
        savelist_label = Label(savelist_frame, text="List Name:")
        #User enters the name of the new list here
        self.savelist_entry = Entry(savelist_frame, width=25)
        #Pressing this button adds the new list to the database
        savelist_button = Button(savelist_frame, text="Save", command = self.save_list)
        savelist_label.pack(side=LEFT)
        savelist_button.pack(side=RIGHT)
        self.savelist_entry.pack(side=RIGHT, padx=5)
        savelist_frame.grid(column=2, row=2, sticky=N, pady=5)

        #Create list panel for browsing the words stored in database
        self.wordbrowse_frame = ListPane(self, height=25) 
        categorymenu_frame.grid(column=0, row=0)
        difficultymenu_frame.grid(column=0, row=2)
        self.wordbrowse_frame.grid(column=0, row=1, sticky=N)
        #Populate the list with words from database
        self.update_category()
        #Double-clicking a word has same effect as transfer button
        self.wordbrowse_frame.doubleClickFunc = self.transfer_right

        #Create list panel for holding/displaying the list being built
        self.userlist_frame = ListPane(self, height=25)
        self.userlist_frame.grid(column=2, row=1, sticky=N)
        #Double-clicking a word has same effect as transfer button
        self.userlist_frame.doubleClickFunc = self.transfer_left

    def transfer_right(self, index=None):
        """Moves a word from word browser to user list"""
        if index == None:
            index = self.wordbrowse_frame.listbox.curselection()
        if self.wordbrowse_frame.get()!=None:
            #Add selection to user list
            selectedWord = self.wordbrowse_frame.get()
            self.userlist_frame.insert(selectedWord)
            #Remove selection from word browser
            word_list = list(self.wordbrowse_frame.getDisplayed())
            word_list.remove(self.wordbrowse_frame.get())
            self.wordbrowse_frame.display(word_list)
            #Ensure current list contents stay visible and select the next word
            self.wordbrowse_frame.listbox.see(index)
            self.wordbrowse_frame.listbox.selection_set(index)
            selectedWordDifficulty = int(db.getWord(selectedWord)[0]["difficulty"][2])
            
            if self.lastSubmittedCategory == "":
                lastSubmittedCategory = db.getWord(selectedWord)[0]["difficulty"]
                self.highest_difficulty = selectedWordDifficulty
            
            elif self.lastSubmittedCategory.startswith("UL"):
                if selectedWordDifficulty > self.highest_difficulty:
                    self.highest_difficulty = selectedWordDifficulty
                    lastSubmittedCategory = lastSubmittedCategory[:1] + selectedWordDifficulty
            else:
                if lastSubmittedCategory != db.getWord(selectedWord)[0]["difficulty"]:
                    lastSubmittedCategory = "UL" + selectedWord[2]  
                elif selectedWordDifficulty > self.highest_difficulty:
                    self.highest_difficulty = selectedWordDifficulty
                    lastSubmittedCategory = lastSubmittedCategory[:1] + selectedWordDifficulty



    def transfer_left(self, index=None):
        """Moves a word from user list back to word browser"""
        if index == None:
            index = self.userlist_frame.listbox.curselection()
        if self.userlist_frame.get()!=None:
            word_list = list(self.wordbrowse_frame.getDisplayed())
            word_list.append(self.userlist_frame.get())
            word_list.sort(key=str.lower)
            self.wordbrowse_frame.display(word_list)
            word_list = list(self.userlist_frame.getDisplayed())
            word_list.remove(self.userlist_frame.get())
            self.userlist_frame.display(word_list)
            self.userlist_frame.listbox.see(index)
            self.userlist_frame.listbox.select_set(index)

    def random_list(self):
        source_list = self.wordbrowse_frame.getDisplayed()
        list_size = int(self.random_list_entry.get())
        if list_size > len(source_list):
            list_size = len(source_list)
            self.random_list_entry.delete(0, END)
            self.random_list_entry.insert(0, list_size)
        generated_list = random.sample(source_list, list_size)
        self.userlist_frame.display(generated_list)

    def update_difficulty(self, event=None):
        """Populates the word browser with words from the selected category"""
        difficulty = self.browse_difficulty_v.get()
        category = self.category_v.get() 
        if category == "Child":
            category = "CL"
        elif category == "ESOL":
            category = "AL"
        elif category == "Spelling Bee":
            category = "SB"
        else: category = "UL"
        word_records = self.db.sql("SELECT word FROM words WHERE difficulty LIKE '%s%s'"%(category, difficulty))
        word_list = []
        for word in word_records:
            word_list.append(str(word[0]))
        word_list.sort(key=str.lower)
        self.wordbrowse_frame.display(word_list)
    
    def update_category(self, event=None):
        """Populates the word browser with words from the selected category"""
        difficulty = self.browse_difficulty_v.get()
        category = self.category_v.get()
        if category == "Child":
            category = "CL"
        elif category == "ESOL":
            category = "AL"
        elif category == "Spelling Bee":
            category = "SB"
        else: category = "UL"
        word_records = self.db.sql("SELECT word FROM words WHERE difficulty LIKE '%s%s'"%(category, difficulty))
        word_list = []
        for word in word_records:
            word_list.append(str(word[0]))
        word_list.sort(key=str.lower)
        self.wordbrowse_frame.display(word_list)

    def add_word(self):
        """Adds a new word to the database with the attributes entered by the user"""
        if self.addword_entry.get() == "":
            return
        self.db.sql("INSERT INTO words (word, definition, usage, difficulty) VALUES ('%s', '%s', '%s', 'UL%s')"%(self.addword_entry.get(), self.adddefinition_entry.get(), self.adduse_entry.get(), self.difficulty_v.get()))
        self.addword_entry.delete(0, END)
        self.adddefinition_entry.delete(0, END)
        self.adduse_entry.delete(0, END)
        #User words are added to a 'Custom' category
        self.category_v.set("Custom")
        self.update_category(None)
        self.db.commit()

    def save_list(self):
        """Adds the list contained in the user list panel to the database"""
        category = self.lastSubmittedCategory
        word_list = self.userlist_frame.getDisplayed()
        self.db.createList(word_list, self.savelist_entry.get(), category)
        self.savelist_entry.delete(0, END)
        self.db.commit()
        
class StatsView(Frame):
    
    def __init__(self, parent, student):
        self.student = student
        Frame.__init__(self, parent)
        self['bg']="#99CCFF"
        self.db = SpellingDatabase()
        self.initGUI()

    def initGUI(self):
        #Clear a students record
        def reset():                        
            self.student.getRecord().reset(self.db)
            lpWords.display([])
            lpLists.display([])
        
        #Display the details and records for the selected word
        def wordListClick(index):
            if len(lpWords.getDisplayed()) == 0:
                return
            wordResults = self.db.sql("""SELECT words.definition, words.usage, words.difficulty, wordCompletion.completed, wordCompletion.attempts 
                    FROM words, wordCompletion 
                    WHERE words.word_id = wordCompletion.word_id 
                    AND words.word = '%s'
                    AND wordCompletion.student_id = '%s'""" % (lpWords.get(index), self.student.getInfo()['id']))[0]
            
            lDefinition.configure(text="Definition: %s" %(wordResults['definition']))
            lUsage.configure(text="Usage: %s" % (wordResults['usage']))
            lDifficulty.configure(text="Difficulty: %s" % (wordResults['difficulty']))
            lCompleted.configure(text="Completed: %s" % (wordResults['completed']))
            lAttempts.configure(text="Attempts: %s" % (wordResults['attempts']))

        #Display the details and records for the selected list    
        def listListClick(index):
            if len(lpLists.getDisplayed()) == 0:
                return
            listResults = self.db.sql("""SELECT lists.source, lists.date_edited, lists.num_words, lists.category, records.best_time, records.high_score, records.completion
                    FROM lists, records 
                    WHERE lists.list_id = records.list_id 
                    AND lists.list_name = '%s'
                    AND records.student_id = '%s'""" % (lpLists.get(index), self.student.getInfo()['id']))[0]
            
            lSource.configure(text="Source: %s" %(listResults['source']))
            lDateEdited.configure(text="Date Edited: %s" % (listResults['date_edited']))
            lSize.configure(text="Size: %s words" % (listResults['num_words']))
            lCategory.configure(text="Category: %s" % (listResults['category']))
            mins = listResults['best_time'] / 60
            secs = listResults['best_time'] % 60
            lTime.configure(text="Best Time: %0d:%02d" % (mins, secs))
            lScore.configure(text="High Score: %d" % (listResults['high_score']))
            lCompletion.configure(text="Completion: %.1f%%" % (listResults['completion']))
        
        
        #Two listPanes and a reset button
        lpLists = ListPane(self, width=20, height=10)
        lpWords = ListPane(self, width=20, height=10)
        bReset = WF.makeButton(self, "Reset", command=reset, width=10)
        #Populate the ListPanes
        listRecords = self.db.sql("""SELECT lists.list_name 
                                     FROM lists, records 
                                     WHERE lists.list_id = records.list_id 
                                     AND records.student_id = '%s'""" % (self.student.getInfo()['id']))
        
        listNames = []
        for row in listRecords:
            listNames.append(row['list_name'])
        lpLists.display(listNames)

        wordRecords = self.db.sql("""SELECT words.word 
                                     FROM words, wordCompletion 
                                     WHERE words.word_id = wordCompletion.word_id 
                                     AND wordCompletion.student_id = '%s'
                                     ORDER BY words.word""" % (self.student.getInfo()['id']))
        wordNames = []
        for row in wordRecords:
            wordNames.append(row['word'])
        lpWords.display(wordNames)
        #Bind single click functions
        lpLists.singleClickFunc = listListClick
        lpWords.singleClickFunc = wordListClick
        #Create four frames for the details and records on words and lists
        fListDetails = Frame(self, bg="#99CCFF")
        #populate the frame
        lListDetails = WF.makeLabel(fListDetails, "List Details")
        lSource = WF.makeLabel(fListDetails,"Source:")
        lDateEdited = WF.makeLabel(fListDetails,"Date Edited: ")
        lSize = WF.makeLabel(fListDetails,"Size:")
        lCategory = WF.makeLabel(fListDetails,"Category:")
        #Pack the Label
        lListDetails.pack(anchor=N)
        lSource.pack(anchor=W)
        lDateEdited.pack(anchor=W)
        lSize.pack(anchor=W)
        lCategory.pack(anchor=W)

        fListRecord = Frame(self, bg="#99CCFF")
        #populate the frame
        lListRecord = WF.makeLabel(fListRecord, "List Record")
        lTime = WF.makeLabel(fListRecord,"Best Time:")
        lScore = WF.makeLabel(fListRecord,"High Score: ")
        lCompletion = WF.makeLabel(fListRecord,"Completion:")
        #Pack the Label
        lListRecord.pack(anchor=N)
        lTime.pack(anchor=W)
        lScore.pack(anchor=W)
        lCompletion.pack(anchor=W)
        
        fWordDetails = Frame(self, bg="#99CCFF")
        #populate the frame
        lWordDetails = WF.makeLabel(fWordDetails, "Word Details")
        lDefinition = WF.makeLabel(fWordDetails,"Definition:")
        lUsage = WF.makeLabel(fWordDetails,"Usage: ")
        lDifficulty = WF.makeLabel(fWordDetails,"Difficulty:")
        #Pack the Label
        lWordDetails.pack(anchor=N)
        lDefinition.pack(anchor=W)
        lUsage.pack(anchor=W)
        lDifficulty.pack(anchor=W)

        fWordRecord = Frame(self, bg="#99CCFF")
        #populate the frame
        lWordRecord = WF.makeLabel(fWordRecord, "Word Record")
        lCompleted = WF.makeLabel(fWordRecord,"Completed:")
        lAttempts = WF.makeLabel(fWordRecord,"Attempts: ")
        #Pack the Label
        lWordRecord.pack(anchor=N)
        lCompleted.pack(anchor=W)
        lAttempts.pack(anchor=W)

        #Grid the frames and pack the self Frame
        lpLists.grid(row=0, column=0, pady=3,padx=3)
        lpWords.grid(row=1, column=0, pady=3,padx=3)
        bReset.grid(row=2, column=0, pady=3, padx=3)
        fListDetails.grid(row=0, column=1, sticky=NW,padx=3)
        fListRecord.grid(row=0, column=2, sticky=NW,padx=3)
        fWordDetails.grid(row=1, column=1, sticky=NW,padx=3)
        fWordRecord.grid(row=1, column=2, sticky=NW,padx=3)


    
        
