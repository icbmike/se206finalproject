#!/bin/bash

# Work out which directory the script is stored in
DIR="$( cd -P "$( dirname "$0" )" && pwd )"
# Tell python where to find the extra modules I've included
export PYTHONPATH=$PYTHONPATH:$DIR
# Change to my directory, in case we were being run from somewhere else
cd $DIR
# Run the project
python launcher.py

