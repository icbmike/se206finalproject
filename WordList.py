#!/usr/bin/env python
class WordList(object):
    
    id=-1
    name= ""
    words = []
    source = ""
    dateEdited = ""
    numWords = -1
    category = ""
    
    def __init__(self, id, name, words, source, date_edited, num_words, category):
        self.id=id
        self.words=words
        self.source=source
        self.dateEdited=date_edited
        self.numWords = num_words
        self.current = 0
        self.category = category
        self.name = name
    def getWords(self):
        return self.words
    
    def getName(self):
        return self.name
    def getCategory(self):
        return self.category
    def getID(self):
        return self.id
    
    def getDifficulty(self):
        return int(self.category[2])

    def getSource(self):
        return self.source
    
    def getDateEdited(self):
        return self.dateEdited
    
    def getNumWords(self):
        return self.numWords
    
    def getCurrentIndex(self):
        return self.current
    
    def setWords(self, words):
        self.words = words
    
    def setSource(self, source):
        self.source = source
    
    def setDateEdited(self, dateEdited):
        self.dateEdited = dateEdited
    
    def setNumWords(self, numWords):
        self.numWords = numWords
    
    def setID(self, id):
        self.id=id

    def __iter__(self):
        return self
    
    def next(self):
        if self.current >= len(self.words):
            raise StopIteration
        else:
            self.current += 1
            return self.words[self.current -1]

class Word(object):
    id = -1
    word = ""
    definition = ""
    usage=""
    difficulty=""

    def __init__(self, word, definition, usage, difficulty, id):
        self.id = id
        self.word = word
        self.definition = definition
        self.usage = usage
        self.difficulty = difficulty

    def getWord(self):
        return self.word

    def getID(self):
        return self.id
    
    def getDefinition(self):
        return self.definition
    
    def getUsage(self):
        return self.usage
    
    def getDifficulty(self):
        return self.difficulty
    
