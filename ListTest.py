#!/usr/bin/env python
from Tkinter import Tk, Button, Label, Entry,Frame, Message, StringVar, TclError, PhotoImage
from Tkconstants import LEFT, TOP, CENTER, END, W,  E
from SpellingDatabase import SpellingDatabase
from Festival import FestivalInterface
from WordList import WordList, Word
import time
class Tester(object):
    def __init__(self, wordList, student, mainWindow, db):
        self.mainWindow=mainWindow
        self.wordList = wordList
        self.student = student
        self.studentRecord = student.getRecord()
        self.current_word = wordList.next()
        self.festival = FestivalInterface()
        self.elapsedTime = 0
        self.keybouncetime = time.time()
        self.wordCompletion = {}
        self.numCompleted = 0
        self.stopTiming = False
        self.after_id = None
        self.db = db

        self.root = Tk()
        self.root.option_add("*Label.Font", "helvetica 12 bold")
        self.root.option_add("*Message.Font", "helvetica 11 normal")
        self.root.option_add("*Button.Font", "helvetica 11 normal")
        self.root.protocol("WM_DELETE_WINDOW", self.cancel)
        self.openingFrame()
        self.root.mainloop()
    def cancel(self):     
        #Destroy window
        if self.after_id is not None:
            self.root.after_cancel(self.after_id)
        self.root.quit()
        self.root.destroy()
        #Commit any completion
        self.calculateStats()
        #Find the time elapsed and use it along with completion and list difficulty to calculate score
        #Check if the time elapsed is a better time
        if self.wordList.getID() in self.studentRecord.getListScores():
            #Check if stored time is better
            if self.studentRecord.getListScores(self.wordList.getID())[0] < self.elapsedTime:
                self.elapsedTime = self.studentRecord.getListScores(self.wordList.getID())[0]
            #Check if calculated score is better than stored score
            if self.studentRecord.getListScores(self.wordList.getID())[1] < self.score:
                self.score = self.studentRecord.getListScores(self.wordList.getID())[1]
            #Check if stored completion is better
            if self.studentRecord.getListScores(self.wordList.getID())[2] < self.completion:
                self.completion = self.studentRecord.getListScores(self.wordList.getID())[2]
        #Update the contents of the Record
        if self.wordList.getID() != -1:
            self.studentRecord.addScore(self.wordList.getID(), self.elapsedTime, self.score, self.completion)
        self.studentRecord.commitRecord(self.db)
        self.student.setRecord(self.studentRecord)
        self.mainWindow.deiconify()
    
    def calculateStats(self):
        self.completion = float(self.numCompleted)/float(self.wordList.getNumWords()) * 100
        self.score = (self.completion * 10 - (self.elapsedTime/self.wordList.getNumWords() - 2.5)*2.5)*(1+ (self.wordList.getDifficulty()-1)*0.25)

    def displayResults(self):
        #Close up the test view and do calculations
        self.fMainFrame.pack_forget()
        self.stopTiming = True
        self.fMainFrame.destroy()
        self.root.after_cancel(self.after_id)
        self.calculateStats()

        if self.wordList.getID() in self.studentRecord.getListScores():
            #Check if stored time is better
            storedTime = self.studentRecord.getListScores(self.wordList.getID())[0] 
            #Check if calculated score is better than stored score
            storedScore = self.studentRecord.getListScores(self.wordList.getID())[1]
            #Check if stored completion is better
            storedCompletion = self.studentRecord.getListScores(self.wordList.getID())[2]
        else:
            storedTime = float("inf")
            storedScore = 0
            storedCompletion = 0
        #Calculate the differences
        diffScore = self.score - storedScore
        diffCompletion = self.completion - storedCompletion
        diffTime = self.elapsedTime - storedTime
        #Create the widgets for the results view
        fResultsFrame = Frame(self.root)
        lTestResults = Label(fResultsFrame,text="Test Results")
        lListName = Label(fResultsFrame,text=self.wordList.getName())
        lListCategory = Label(fResultsFrame,text=self.wordList.getCategory())
        bDone = Button(fResultsFrame, text="Done", command=self.cancel)
        lLabelScore = Label(text="Score: ")
        lLabelCompletion = Label(text="Completion(%): ")
        lLabelTime = Label(text="Time(s): ")
        #Create a separate frame for the actual results
        fResultsTable = Frame(fResultsFrame)
        lLabelScore = Label(fResultsTable,text="Score: ")
        lLabelCompletion = Label(fResultsTable,text="Completion(%): ")
        lLabelTime = Label(fResultsTable,text="Time(s): ")
        lThis = Label(fResultsTable, text="This Time")
        lLast = Label(fResultsTable, text="Current Best")
        lThisScore = Label(fResultsTable, text=int(self.score))
        lLastScore = Label(fResultsTable, text=int(storedScore))
        lThisCompletion = Label(fResultsTable, text=int(self.completion)) 
        lLastCompletion = Label(fResultsTable, text=int(storedCompletion))
        esecs = self.elapsedTime % 60
        emins = self.elapsedTime / 60
        lThisTime = Label(fResultsTable, text="%2d:%2d" % (emins,esecs))
        if storedTime == float("inf"):
            lLastTime = Label(fResultsTable, text="--:--")
        else:
            secs = storedTime % 60
            mins = storedTime / 60
            lLastTime = Label(fResultsTable, text="%2d:%2d" % (mins, secs))
        lDiffScore = Label(fResultsTable)
        lDiffCompletion = Label(fResultsTable)
        lDiffTime = Label(fResultsTable)
        #Configure the diff labels
        if diffScore > 0:
            lDiffScore.configure(fg="#00AA00", text="+%d" % (diffScore))
        elif diffScore == 0:
            lDiffScore.configure(fg="black", text=str(diffScore))
        else:
            lDiffScore.configure(fg="red", text="-%s" %(diffScore))
        
        if diffCompletion > 0:
            lDiffCompletion.configure(fg="#00AA00", text="+%d" % (diffCompletion))
        elif diffCompletion == 0:
            lDiffCompletion.configure(fg="black", text=str(diffCompletion))
        else:
            lDiffCompletion.configure(fg="red", text="-%s" %(diffCompletion))
        
        if diffTime < 0:
            if diffTime == -float("inf"):
                lDiffTime.configure(fg="#00AA00", text="--:--")
            else:
                lDiffTime.configure(fg="#00AA00", text="%d" % (diffTime))
        elif diffTime == 0:
            lDiffTime.configure(fg="black", text=str(diffTime))
        else:
            lDiffTime.configure(fg="red", text="-%s" %(diffTime))
        #Grid the results table
        lThis.grid(row=0,column=1)
        lLast.grid(row=0, column=3)
        lLabelScore.grid(row=1, column=0, sticky=W)
        lThisScore.grid(row=1, column=1)
        lDiffScore.grid(row=1, column=2)
        lLastScore.grid(row=1, column=3)
        lLabelCompletion.grid(row=2, column=0, sticky=W)
        lThisCompletion.grid(row=2, column=1)
        lDiffCompletion.grid(row=2, column=2)
        lLastCompletion.grid(row=2, column=3)
        lLabelTime.grid(row=3, column=0, sticky=W)
        lThisTime.grid(row=3, column=1)
        lDiffTime.grid(row=3, column=2)
        lLastTime.grid(row=3, column=3)
        
        #Grid the main widgets
        lTestResults.grid(row=0, column=0, columnspan=2)
        lListName.grid(row=1, column=0)
        lListCategory.grid(row=1, column=1)
        fResultsTable.grid(row=3, column=0, columnspan=2, padx=5, pady=3)
        bDone.grid(row=4, column=0, columnspan=2)
        fResultsFrame.pack()

    def submit(self, event=None):
        #Need to prevent "Key bouncing" so that festival doesn't freak out
        #Unfortunately even with a 2 second time limit enough information 
        #can be sent to festival to make it crash.
        #However this only happens when the Enter key is held down.
        now = time.time()
        if (now - self.keybouncetime) < 2:
            return
        self.keybouncetime = now
        if self.current_word.getID() in self.studentRecord.getWordCompletion():
            if str(self.studentRecord.getWordCompletion()[self.current_word.getID()]) == "True":
                attempts = self.studentRecord.getWordCompletion(self.current_word.getID())[1]
                dont_update = True
            else:
                attempts = self.studentRecord.getWordCompletion(self.current_word.getID())[1] + 1
                dont_update=False
        else:
            attempts = 1
            dont_update=False
        #Check if the word entered was correct
        wordAttempt = self.eWordEntry.get()
        if wordAttempt == self.current_word.getWord():
            self.eWordEntry.configure(bg="green")
            self.lMisspeltWord.configure(text="")
            self.numCompleted += 1
            #Modify the record for the attempted word
            self.studentRecord.addCompletion(self.current_word.getID(),
                                             True,
                                             attempts)
        else:
            self.eWordEntry.configure(bg="red")
            self.lMisspeltWord.configure(text="Correct spelling: '%s'"%(self.current_word.getWord()))
            #Store completion in record
            self.studentRecord.addCompletion(self.current_word.getID(),
                                             True if dont_update else False, 
                                             attempts) 
        #Increment through wordList
        try:
            self.current_word = self.wordList.next()
            self.lProgress.configure(text="%d/%d" %(self.wordList.getCurrentIndex(), self.wordList.getNumWords()))
            self.eWordEntry.delete(0,END)
            self.eWordEntry.focus_force()
            self.speak()
        except StopIteration:
            self.displayResults()
        
    
    def speak(self):
        self.festival.speech(self.current_word.getWord())

    def mainFrame(self):
        #Destroy the opening frame
        self.fInitialFrame.destroy()
        
        """Creating the main frame and its widgets. 
        Bind the return key on the entry to the same action as the submit Button"""
        self.fMainFrame = Frame(self.root)
        self.lTime = Label(self.fMainFrame, text="Time Elapsed: 00:00")
        self.lProgress = Label(self.fMainFrame, text="%d/%d" % (self.wordList.getCurrentIndex(), self.wordList.getNumWords()))
        self.lMisspeltWord = Label(self.fMainFrame)
        photo = PhotoImage(master=self.fMainFrame, file="speaker.gif") 
        bSpeak = Button(self.fMainFrame,image=photo,width=60, height=60, command=self.speak)
        bSpeak.photo = photo
        self.eWordEntry = Entry(self.fMainFrame,width=30)
        self.eWordEntry.bind("<Return>", self.submit)
        bSubmit = Button(self.fMainFrame,text="Check!", command=self.submit)
        bSkip = Button(self.fMainFrame, text="I Don't Know", command=self.submit)
        bReturn = Button(self.fMainFrame,text="Exit", command=self.cancel)
        #Grid the elements
        self.lTime.grid(row=0, column=1)
        self.lProgress.grid(row=0, column=2)
        self.lMisspeltWord.grid(row=0, column=0, pady=3)
        bSpeak.grid(row=1, column=0, rowspan=2)
        self.eWordEntry.grid(row=1, column=1, padx=3)
        bSubmit.grid(row=1,column=2)
        bSkip.grid(row=2,column=2, pady=3)
        bReturn.grid(row=2, column=1)
        self.fMainFrame.pack(padx=10,pady=7)
        #Speak the first Word
        self.speak()
        #Schedule the timeupdate
        self.after_id = self.root.after(1000, self.timeIncr)
        self.eWordEntry.focus_force() 
    def timeIncr(self):
        self.elapsedTime +=1
        minutes = self.elapsedTime / 60
        seconds = self.elapsedTime % 60
        try:
            self.lTime.configure(text="Time Elapsed: %02d:%02d" % (minutes, seconds))
        except TclError:
            pass
        if not self.stopTiming:
            self.root.after(1000, self.timeIncr)

    def openingFrame(self):
        """Creating the opening frame. Will have two Buttons,
        one to return to the previous screen and the other to begin the test."""
        self.fInitialFrame = Frame(self.root)
        mMessage = Message(self.fInitialFrame, 
                text="The test is timed for scoring purposes. Press the Start button to begin when ready.", width=250)
        fButtonFrame = Frame(self.fInitialFrame)
        bStart = Button(fButtonFrame, text="Start",width=10, command=lambda: self.mainFrame())
        bReturn = Button(fButtonFrame, text="Back", width=10, command=self.cancel)
        #Pack the elements and the frame
        mMessage.pack(pady=3)
        bStart.pack(side=LEFT,padx=3)
        bReturn.pack(side=LEFT)
        fButtonFrame.pack()
        self.fInitialFrame.pack(padx=20, pady=10)


