from Tkinter import Button, Entry, Label, Message
from Tkinter import FLAT


class WidgetFactory(object):

    @staticmethod
    def makeButton(parent, text, width=15, height=0, fontSize=14):
        BG = "#99CCFF"
        hBG = "#55CCFF"
        FG = "#336677"
        hFG = "#55CCFF"
        
        return Button(parent=parent,
                      text=text, 
                      width=width,
                      height=height,
                      bg=BG,
                      bd=3,
                      font=("Helvetica", fontSize, "bold"),
                      activebackground = hBG,
                      highlightbackground = FG,
		      highlightcolor = hFG,
		      highlightthickness = 4,
                      relief=FLAT)

    def makeLabel(parent, text, fontSize=12, fontStyle="normal"):
        BG = "#99CCFF"

        return Label(parent=parent,
                     texxt=text,
                     font=("Helvetica", fontSize, fontStyle))
