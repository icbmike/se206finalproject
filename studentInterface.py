#!/usr/bin/env python
from Tkinter import Tk, Button, Entry, Label, Frame, Message, StringVar, OptionMenu
from Tkconstants import RIGHT, X, SUNKEN, LEFT, TOP, Y, NW, N, W, FLAT
import hashlib, tkFont, random
from Classes import Word, WordList
from Views import Tester, PractiseView, StatsView
from Utils import SpellingDatabase, ListPane, WidgetFactory as WF

class StudentInterface(object):
    
    def __init__(self):
        self.db = SpellingDatabase()
        self.student=None
        self.loginScreen()

    def mainApp(self):
        def logout():
            self.root.destroy()
            self.student=None
            self.loginScreen()

        self.root = Tk()
        self.root['bg'] = "#99CCFF"
        self.root.title("Spelling Aid")
        self.root.protocol("WM_DELETE_WINDOW", logout)
        self.fWelcomeFrame = Frame(self.root, relief=FLAT, bd=1)
        self.fWelcomeFrame['bg'] = "#99CCFF"

        #Top menu frame, holds username and a logout button
        self.fTopMenu = Frame(self.fWelcomeFrame, bd=2, relief=SUNKEN, bg="#99CCFF")
        bLogout = WF.makeButton(self.fTopMenu, "Logout", fontSize=10, width=8)
        bLogout['command'] = logout
        lUsername = WF.makeLabel(self.fTopMenu, self.student.getInfo()['fname'] + " " + self.student.getInfo()['lname'], fontSize=10, fontStyle="bold")
        #Pack the elements
        bLogout.pack(side=RIGHT)
        lUsername.pack(side=RIGHT, padx=5)
        
        #Content Frame
        self.fContent = Frame(self.fWelcomeFrame, bg="#99CCFF",width=400, height=350)
        self.fContent.pack_propagate(0)
        bPractiseSpelling = WF.makeButton(self.fContent, "Practise!", command=self.selectList, width=10)
        mPractiseSpelling = WF.makeMessage(self.fContent,"Practise premade spelling lists or randomly generated lists",width=250 )
        bViewProgress = WF.makeButton(self.fContent,"View Stats", width=10,command=self.viewStats)
        mViewProgress = WF.makeMessage(self.fContent,"View your progress and stats", width=250)
        #Grid these Widgets
        mPractiseSpelling.pack(pady=5)
        bPractiseSpelling.pack(pady=5)
        mViewProgress.pack(pady=5)
        bViewProgress.pack(pady=5)
        
        #Pack the frames
        self.fTopMenu.pack(side=TOP,fill=X, expand=True, anchor=N)
        self.fContent.pack()
        self.fWelcomeFrame.pack()


        self.root.mainloop()

    def loginScreen(self):
        def submit(event=None):
            username = eUsername.get()
            password = hashlib.sha1(ePassword.get()).hexdigest()
            student_result = self.db.login(username, password)
            if student_result == None:
                mError.pack(pady=5)
            else:
                self.student = student_result
                login_root.destroy()
                if self.student != None:
                    self.mainApp()
         
        
        login_root = Tk()
        login_root['bg'] = "#99CCFF"
        login_root.title("Spelling Aid")
        fContainer = Frame(login_root, bg="")
        lUsername = WF.makeLabel(fContainer, "Username:")
        lPassword = WF.makeLabel(fContainer,"Password:")
        eUsername = WF.makeEntry(fContainer)
        ePassword = WF.makeEntry(fContainer, show="*")
        mError = WF.makeMessage(fContainer, "The details you entered were incorrect. Try Again",width=200)
        mError['fg'] = 'red'
        bSubmit = WF.makeButton(fContainer,"Submit",command=submit)
        bLeave = WF.makeButton(fContainer, "Leave", command = lambda: exit())
        login_root.bind("<Return>", submit)
        eUsername.focus_force()
        lUsername.pack()
        eUsername.pack()
        lPassword.pack()
        ePassword.pack()
        bSubmit.pack(pady=5)
        bLeave.pack()
        fContainer.pack(pady=20, padx=20)
        
        login_root.mainloop()
   
    def selectList(self):
        def back():
            fSelectList.destroy()
            self.fContent.pack(padx=20,pady=5)
            bBack.pack_forget()

        #Add 'back' button to top menu frame
        fSelectList = PractiseView(self.fWelcomeFrame, self.student)
        self.fContent.pack_forget()
        fSelectList.pack()
        bBack = WF.makeButton(self.fTopMenu, "Back", command=back, fontSize=10, width=8)
        bBack.pack(side=LEFT)
     
            
    def viewStats(self):
        def back():
            fStatsView.destroy()
            self.fContent.pack(padx=20,pady=5)
            bBack.pack_forget()

        #Add 'back' button to top menu frame
        bBack = WF.makeButton(self.fTopMenu, "Back", command=back, fontSize=10, width=8)
        bBack.pack(side=LEFT)
        fStatsView = StatsView(self.fWelcomeFrame, self.student)
        self.fContent.pack_forget()
        fStatsView.pack()

if __name__ == "__main__":
    StudentInterface()
