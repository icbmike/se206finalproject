#!/usr/bin/env python
from Tkinter import Tk, Button, Entry, Label, Frame, Message, StringVar, OptionMenu
from Tkconstants import RIGHT, X, SUNKEN, LEFT, Y, NW, N, W
from SpellingDatabase import SpellingDatabase
from ListPane import ListPane
from WordList import WordList, Word 
from ListTest import Tester
class PractiseView(Frame):
    def __init__(self, parent, student):
        self.student = student
        Frame.__init__(self, parent)
        self.db = SpellingDatabase()
        self.initGUI()
    
    def initGUI(self):
        def displayInfoAll(index):
                selectedList = self.db.getList(lpAll.get(index))[0]
                mListInfo.configure(text="List Name: %s\nSource: %s\nDate Edited: %s\nSize: %s words\nCategory: %s"%(selectedList['list_name'], selectedList['source'], selectedList['date_edited'], selectedList['num_words'], selectedList['category']))
            
        def displayInfoRecommended(index):
                selectedList = self.db.getList(lpRecommended.get(index))[0]
                mListInfo.configure(text="List Name: %s\nSource: %s\nDate Edited: %s\nSize: %s words\nCategory: %s"%(selectedList['list_name'], selectedList['source'], selectedList['date_edited'], selectedList['num_words'], selectedList['category']))
        
        def practiseSelected(event=None):
            #Get whats selected
            allSelection = lpAll.get()
            recommendedSelection = lpRecommended.get()
            #Make sure that something atually is selected and pass the element to a listTest()
            if allSelection is not None:
                self.listTest(allSelection)
            elif recommendedSelection is not None:
                self.listTest(recommendedSelection)
            else: 
                return
        
        #Create the select list view frame
        self.option_add("*Label.Font", "helvetica 14 bold")
        self.option_add("*Message.Font", "helvetica 12")
        self.option_add("*Button.Font", "helvetica 12")
        self.option_add("*OptionMenu.Font", "helvetica 12")
        #create left and right frames
        fLeft = Frame(self)
        fRight = Frame(self)
        #Create the ListPanes for selecting lists
        lRecommended = Label(fLeft, text="Recommended Lists:")
        lpRecommended = ListPane(fLeft, height=10, width=20)
        lpRecommended.singleClickFunc = displayInfoRecommended
        lAll = Label(fLeft, text="All Lists:")
        lpAll = ListPane(fLeft, height=10, width=20)
        lpAll.singleClickFunc = displayInfoAll
        #Populate the list panes
        allListResults = self.db.getLists()
        allListNames = []
        for row in allListResults:
            allListNames.append(row['list_name'])
        lpAll.display(allListNames)
        if self.student.getInfo()['category'] == "Child":
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'CL%'")
        elif self.student.getInfo()['category'] == "ESOL":
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'AL%'")
        else:
            recommendedListResults = self.db.sql("SELECT * FROM lists WHERE category LIKE 'SB%'")
        recommendedListNames = []
        for row in recommendedListResults:
            recommendedListNames.append(row['list_name'])
        lpRecommended.display(recommendedListNames)
        #pack the left hand widgets
        lRecommended.pack()
        lpRecommended.pack()
        lAll.pack()
        lpAll.pack()
        #Create the righ hand widgets
        mListInfo = Message(fRight, width=300, text="Select a list")
        bDoSelected = Button(fRight, text="Practise List!", command=practiseSelected)
        mRandom = Message(fRight, width=3000, text="Practise a random up to 30 word list from the selected category")
        fComboButtonContainer = Frame(fRight)
        category_options = ['AL1', 'AL2', 'CL1', 'CL2', 'CL3', 'CL4', 'CL5', 'CL6', 'CL7', 'CL8', 'SB1', 'UL1']
        self.category_variable = StringVar(fComboButtonContainer)
        self.category_variable.set(category_options[0])
        cbCategory = OptionMenu(fComboButtonContainer, self.category_variable, *category_options)
        bRand = Button(fComboButtonContainer, text="Practise Random List", command=self.randList)
        #pack the right hand widgets
        cbCategory.pack(side=RIGHT)
        bRand.pack(side=RIGHT)
        mListInfo.pack(anchor=NW)
        bDoSelected.pack()
        mRandom.pack()
        fComboButtonContainer.pack()
        #Pack widgets
        fLeft.pack(side=LEFT, padx=5, pady=3)
        fRight.pack(side=LEFT, pady=3, fill=Y)
            
    def randList(self):
        self.listTest("", rand=True, category=self.category_variable.get())
    
    def listTest(self, listName, rand=False, category=""):
        if rand == True:
            print category
            wordResult = self.db.sql("SELECT * FROM words WHERE difficulty = '%s'" % (category)) 
            if len(wordResult) > 30:
                random.shuffle(wordResult)
                wordResult = wordResult[:30]
            
            words = []
            for row in wordResult:
                words.append(Word(row["word"], row["definition"], row["usage"], row["difficulty"], row["word_id"]))
            
            wordList = WordList(-1, "30 Random %s Words" % (category), words, "Random", "Right Now", len(wordResult), category)
        else:
            listResult = self.db.getList(listName)[0]
            wordResult = self.db.getWords(listName)
            words = []
            for row in wordResult:
                words.append(Word(row["word"], row["definition"], row["usage"], row["difficulty"], row["word_id"]))

            wordList = WordList(listResult["list_id"], listResult["list_name"],words, listResult["source"], listResult["date_edited"], listResult["num_words"], listResult["category"])
        
        self.winfo_toplevel().withdraw()
        Tester(wordList, self.student, self.winfo_toplevel(), self.db)

