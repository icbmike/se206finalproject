#!/usr/bin/env python
from Tkinter import Tk, Button, Entry, Label, Frame, Message, StringVar, OptionMenu
from Tkconstants import RIGHT, X, SUNKEN, LEFT, Y, NW, N, W
from SpellingDatabase import SpellingDatabase
from ListPane import ListPane
class StatsView(Frame):
    
    def __init__(self, parent, student):
        self.student = student
        Frame.__init__(self, parent)

        self.db = SpellingDatabase()
        self.initGUI()

    def initGUI(self):
        
        def wordListClick(index):
                wordResults = self.db.sql("""SELECT words.definition, words.usage, words.difficulty, wordCompletion.completed, wordCompletion.attempts 
                        FROM words, wordCompletion 
                        WHERE words.word_id = wordCompletion.word_id 
                        AND words.word = '%s'""" % (lpWords.get(index)))[0]
                
                lDefinition.configure(text="Definition: %s" %(wordResults['definition']))
                lUsage.configure(text="Usage: %s" % (wordResults['usage']))
                lDifficulty.configure(text="Difficulty: %s" % (wordResults['difficulty']))
                lCompleted.configure(text="Completed: %s" % (wordResults['completed']))
                lAttempts.configure(text="Attempts: %s" % (wordResults['attempts']))

            
        def listListClick(index):
                listResults = self.db.sql("""SELECT lists.source, lists.date_edited, lists.num_words, lists.category, records.best_time, records.high_score, records.completion
                        FROM lists, records 
                        WHERE lists.list_id = records.list_id 
                        AND lists.list_name = '%s'""" % (lpLists.get(index)))[0]
                
                lSource.configure(text="Source: %s" %(listResults['source']))
                lDateEdited.configure(text="Date Edited: %s" % (listResults['date_edited']))
                lSize.configure(text="Size: %s words" % (listResults['num_words']))
                lCategory.configure(text="Completed: %s" % (listResults['category']))
                mins = listResults['best_time'] / 60
                secs = listResults['best_time'] % 60
                lTime.configure(text="Best Time: %2d:%2d" % (mins, secs))
                lScore.configure(text="High Score: %s" % (listResults['high_score']))
                lCompletion.configure(text="Completion: %s" % (listResults['completion']))
        
        
        #Two listPanes
        lpLists = ListPane(self, width=20, height=10)
        lpWords = ListPane(self, width=20, height=10)
        #Populate the ListPanes
        listRecords = self.db.sql("""SELECT lists.list_name 
                                     FROM lists, records 
                                     WHERE lists.list_id = records.list_id 
                                     AND records.student_id = '%s'""" % (self.student.getInfo()['id']))
        
        listNames = []
        for row in listRecords:
            listNames.append(row['list_name'])
        lpLists.display(listNames)

        wordRecords = self.db.sql("SELECT words.word FROM words, wordCompletion WHERE words.word_id = wordCompletion.word_id AND wordCompletion.student_id = '%s'" % (self.student.getInfo()['id']))
        wordNames = []
        for row in wordRecords:
            wordNames.append(row['word'])
        lpWords.display(wordNames)
        #Bind single click functions
        lpLists.singleClickFunc = listListClick
        lpWords.singleClickFunc = wordListClick
        #Create four frames for the details and records on words and lists
        fListDetails = Frame(self)
        #populate the frame
        lListDetails = Label(fListDetails, text="List Details")
        lSource = Label(fListDetails,text="Source:")
        lDateEdited = Label(fListDetails,text="Date Edited: ")
        lSize = Label(fListDetails,text="Size:")
        lCategory = Label(fListDetails,text="Category:")
        #Pack the Label
        lListDetails.pack(anchor=N)
        lSource.pack(anchor=W)
        lDateEdited.pack(anchor=W)
        lSize.pack(anchor=W)
        lCategory.pack(anchor=W)

        fListRecord = Frame(self)
        #populate the frame
        lListRecord = Label(fListRecord, text="List Record")
        lTime = Label(fListRecord,text="Best Time:")
        lScore = Label(fListRecord,text="High Score: ")
        lCompletion = Label(fListRecord,text="Completion:")
        #Pack the Label
        lListRecord.pack(anchor=N)
        lTime.pack(anchor=W)
        lScore.pack(anchor=W)
        lCompletion.pack(anchor=W)
        
        fWordDetails = Frame(self)
        #populate the frame
        lWordDetails = Label(fWordDetails, text="List Record")
        lDefinition = Label(fWordDetails,text="Definition:")
        lUsage = Label(fWordDetails,text="Usage: ")
        lDifficulty = Label(fWordDetails,text="Difficulty:")
        #Pack the Label
        lWordDetails.pack(anchor=N)
        lDefinition.pack(anchor=W)
        lUsage.pack(anchor=W)
        lDifficulty.pack(anchor=W)

        fWordRecord = Frame(self)
        #populate the frame
        lWordRecord = Label(fWordRecord, text="Word Record")
        lCompleted = Label(fWordRecord,text="Completed:")
        lAttempts = Label(fWordRecord,text="Attempts: ")
        #Pack the Label
        lWordRecord.pack(anchor=N)
        lCompleted.pack(anchor=W)
        lAttempts.pack(anchor=W)

        #Grid the frames and pack the self Frame
        lpLists.grid(row=0, column=0, pady=3,padx=3)
        lpWords.grid(row=1, column=0, pady=3,padx=3)
        fListDetails.grid(row=0, column=1, sticky=NW,padx=3)
        fListRecord.grid(row=0, column=2, sticky=NW,padx=3)
        fWordDetails.grid(row=1, column=1, sticky=NW,padx=3)
        fWordRecord.grid(row=1, column=2, sticky=NW,padx=3)


    
        
